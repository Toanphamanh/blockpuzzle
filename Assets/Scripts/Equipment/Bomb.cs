﻿
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class Bomb : Equipment  , IPooledObject {
	[SerializeField]Image moveCountImg ;
	[SerializeField] int moveCount ; 
	float isBombBlast ;
	public bool isCanRemove  ;
	public static UnityEvent PlayerSetMoveEvent ;
	static Sprite[] font ;
	// Use this for initialization
	protected override void Init()
	{
		base.Init();
	}

	void OnPlayerSetMove()
	{
		if(gameObject.activeSelf && moveCount >= 1)
		{
			if(moveCount == 1)
			{
				GameManager.instance.game.isBoomRemove(this) ;
			}
			moveCount--;
			moveCountImg.sprite = font[moveCount];
			
		}
		else  return ;
		
		if(moveCount <= 0 && !isCanRemove) 
		{
			GameManager.instance.currentInfo.gameEnd = GameEnd.BombBlast ;
			GameManager.instance.currentInfo.gameNotice = GameNotice.BombBlast ;
			AudioManager.instance.play("BoomBreak");
			Vector2Int size = GameManager.instance.size ;
			GameManager.instance.currentInfo.gameData[pos.x * size.y + pos.y].type = EquipmentType.Normal ;
			GameManager.instance.GamePlayStatusChangeEvent.Invoke(GameStatus.End) ;
			OnRemove();
		}
	}
	public override void doEffect()
	{
		AudioManager.instance.play("BoomEat");
		OnRemove() ;
	}
	public void OnSpawn()
	{
		Init();
		if(font == null) font = PopUpManager.instance.numberSpritesList[5] ;
		if(moveCount > 9) 
			moveCountImg.sprite = font[moveCount - 1];
		else moveCountImg.sprite = font[moveCount];
		if(PlayerSetMoveEvent == null)
		{
			PlayerSetMoveEvent = new UnityEvent();
		} 
		PlayerSetMoveEvent.AddListener(OnPlayerSetMove);
		isCanRemove = false ;
		if(moveCount == 0) OnBoomBreak();
	}
	public void setMoveCount(int count)
	{
		moveCount = count ;
	}

	public int getMoveCount()
	{
		return moveCount ;
	}


    void OnRemove()
    {
        Equipment.currentEquipmentPosList.Remove(this.pos);
		GameManager.instance.game.bombList.Remove(this.pos);
		OnBoomBreak();
    }

	void OnBoomBreak()
	{
		GameObject bombEffect = ObjectPooler.instance.SpawnFromPool("BombEffect" , transform.position) ;
		ParticleSystem ps = bombEffect.GetComponent<ParticleSystem>();
		if(ps.isPlaying) ps.Pause() ;
		ps.Play();
		AudioManager.instance.play("BoomBreak");
		gameObject.SetActive(false);
	}

    public void OnDespawn()
    {
    }
}
