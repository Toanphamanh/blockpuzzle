﻿using System.Collections.Generic;
using UnityEngine;

public class RocketIdle : Equipment , IPooledObject
{
    //static GameObject rocketPref ;
    [SerializeField] Animator anim;
    bool isQuit , isDoEffect ;
    static List<Vector3> dir ;
    static List<Quaternion> rotate ;
    // Use this for initialization
    void Start()
    {
       if(dir == null && rotate == null)
       {
           dir = new List<Vector3>();
           dir.Add(Vector3.up) ;
           dir.Add(Vector3.right) ;
           dir.Add(Vector3.down) ;
           dir.Add(Vector3.left);
           rotate = new List<Quaternion>();
           rotate.Add(Quaternion.identity) ;
           rotate.Add( Quaternion.Euler(0,0,270));
           rotate.Add( Quaternion.Euler(0,0,180));
           rotate.Add( Quaternion.Euler(0,0,90));
       } 
    }

    protected override void Init()
    {
        base.Init();
    }

    // Update is called once per frame

    public void OnSpawn()
    {
        Init();
        isDoEffect = false;
    }
    public override void doEffect()
    {
        if(isDoEffect) return ;
        isDoEffect = true ;
        if(Equipment.currentEquipmentPosList != null) Equipment.currentEquipmentPosList.Remove(this.pos);
        Debug.Log("Rocket " + gameObject.name + pos.ToString() + "do effect" ) ;
        for(int i = 0 ; i < 4 ; i++)
        {
            GameObject rocket = ObjectPooler.instance.SpawnFromPool("Rocket",transform.position);
            Rocket rk = rocket.GetComponent<Rocket>();
            rk.dir = dir[i] ;
            rk.transform.rotation = rotate[i] ;
            rk.OnSpawn();
        }
        AudioManager.instance.play("Rocket");

        GameObject ob = ObjectPooler.instance.SpawnFromPool("ClearHandler",transform.position);
        //ob.transform.SetParent(UIManager.instance.equipmentT);
        ClearHandler ch = ob.GetComponent<ClearHandler>();
        ch.pos = this.pos;
        ch.OnSpawn();
        ch.OnRocketClear();

        gameObject.SetActive(false);
    }

    public void OnDespawn()
    {
        throw new System.NotImplementedException();
    }
}
