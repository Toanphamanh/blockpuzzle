﻿
using UnityEngine;

public class FlyItem : MonoBehaviour
{
    public Vector3 target ;
    Rigidbody2D rd ;
    Vector2  v0 , a0 , distance ;
    float   t;
    StopWatch sw ;
    [SerializeField ] float h ;
    bool isStart;
    void Start()
    {
        isStart = false ;
    }
    public void OnStart()
    {
        isStart = true ;
        float x = transform.position.x ;
        float dir = x > target.x ? -1 : 1 ;
        v0 = Vector2.one * 2f ;
        v0 = new Vector2(v0.x * dir , v0.y);
        rd = gameObject.GetComponent<Rigidbody2D>() ;
        rd.velocity = v0 ;
        rd.gravityScale = 0f ;
        h = 14f;
        sw = GetComponent<StopWatch>() ;
    }

    private void FixedUpdate()
    {
        if(!isStart) return ;
        t += Time.deltaTime ;
        distance = target - transform.position ;
        if(distance.magnitude > 0.4f)
        {
            a0 =  distance.normalized * h  ;
            rd.velocity = v0 + a0 * t ;
        }
        else
        {
            rd.velocity = Vector2.zero ;
            sw.OnDelivery();
            Destroy(rd);
            Destroy(this);
        } 
    }

    public void OnEnd()
    {
        isStart = false ;
    }
}
