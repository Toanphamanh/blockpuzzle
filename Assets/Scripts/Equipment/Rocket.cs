﻿
using UnityEngine;

public class Rocket : MonoBehaviour , IPooledObject {
	[SerializeField]static float speed = 8f; 
	public Vector3 dir ;
	// Use this for initialization
	float lifeTime ;
	void Start () {
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		transform.position += speed * Time.deltaTime * dir ;
		lifeTime -= Time.deltaTime ;
		if(lifeTime < 0) gameObject.SetActive(false);
	}

    public void OnSpawn()
    {
		lifeTime = 3f;
    }

    public void OnDespawn()
    {
        throw new System.NotImplementedException();
    }
}
