﻿
using UnityEngine;

using DG.Tweening ;
public class StopWatch : Equipment , IPooledObject {
	[SerializeField]
	float lifeTime ;
	int bonusTime ;
	[SerializeField]
	Sprite numberSprite ;
	int minus , seconds ;
	bool isBonus ;
	Animator anim ;
	[SerializeField] Transform target ;
	TextNumberHelper textHelper ;
	// Use this for initialization
	FlyItem fly;
	float time = 0;
	void Awake()
	{
		textHelper = GetComponentInChildren<TextNumberHelper>() ;
		anim = GetComponent<Animator>();
		fly = gameObject.GetComponent<FlyItem>();
	}
    public void OnDelivery()
    {
        GameManager.instance.currentInfo.timeLeft += this.bonusTime ;
		gameObject.SetActive(false) ;
		UIManager.TimeBonusEvent.Invoke(this.bonusTime);
		fly.OnEnd();
    }

	public void BeginFly()
	{
		this.bonusTime = textHelper.value ;
		textHelper.StopCountDown();
		Sequence s = DOTween.Sequence();
		s.Append(transform.DOShakeRotation(1f , 20f , 10 , 30))
		   	.Append(transform.DOMoveY(  transform.position.y + 0.8f,0))
		   		.AppendCallback( () => 
			   {
					target = UIManager.instance.timerIcon ;
					fly.target = target.position ;
					fly.OnStart();
			   });			   
	}
    // Update is called once per frame
    void FixedUpdate () {
		
		if(PopUpManager.instance.isOnRevive || !GameManager.instance.isGamePlay || isBonus)
		{
			return ;
		} 
		else
		{
			//textHelper.StartCountDown();
		}
		time += Time.deltaTime ;
		if(time > 1) 
		{
			lifeTime -- ;
			time = 0 ;
			textHelper.value = (int)lifeTime ;
			textHelper.Init();
			textHelper.Display();
		}
		
		if((int)lifeTime % 4 == 0) anim.SetTrigger("play");
		if(lifeTime <= 0)	OnDespawn();
		//numberSprite = PopUpManager.instance.numberSpritesList[5][seconds];
	}
	public override void doEffect()
	{
		isBonus = true ;
		lifeTime = (int)lifeTime ;
		if(Equipment.currentEquipmentPosList != null) Equipment.currentEquipmentPosList.Remove(this.pos);
		if(lifeTime > 0)
		{
			BeginFly();
		} 
	}
	public void setTime(int time)
	{
		lifeTime = time ;
		textHelper.value = time ;
		textHelper.Init();
		textHelper.Display();
	}

	void DoShake()
	{
	}
	public int getTime()
	{
		return Mathf.CeilToInt(lifeTime);
	}
	protected override void Init()
	{
		base.Init();
	}
	public void OnSpawn()
	{
		Init();
		textHelper.value = (int)lifeTime ;
		textHelper.Init();
		textHelper.Display() ;
		textHelper.setUp();
		isBonus = false ;
	}
    public void OnDespawn()
    {
        
		Vector2Int size = GameManager.instance.size ;
		GameManager.instance.currentInfo.gameData[pos.x * size.y + pos.y].type = EquipmentType.Normal ;
		gameObject.SetActive(false);
    }
}
