﻿
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public abstract class  Equipment : MonoBehaviour  {
	public static UnityEventPlayerCheck OnPlayerCheckClearEvent , OnPlayerClearEvent;
	public static List<Vector2Int> currentEquipmentPosList ;
	public Vector2Int pos ;
	int row , col ;
	
	protected virtual void Init()
	{	
		if(OnPlayerCheckClearEvent == null)
		{
			OnPlayerCheckClearEvent = new UnityEventPlayerCheck() ;
		}
		if(OnPlayerClearEvent == null)
		{
			OnPlayerClearEvent = new UnityEventPlayerCheck() ;
		}		

		if(currentEquipmentPosList == null)
		{
			currentEquipmentPosList = new List<Vector2Int>();
		}
		
		row = pos.y ;
		col = pos.x ;
		currentEquipmentPosList.Add(this.pos);
	}

    
    void Start()
	{
		
	}
	

    
	public abstract void doEffect() ;

    

    public class UnityEventPlayerCheck : UnityEvent<List<int>,List<int>> 
	{

	}

}

