﻿
using UnityEngine;

public class RocketLaunch : MonoBehaviour , IPooledObject
{
    static float timeLife = 2.5f ;
    // Start is called before the first frame update
    float time ;
    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        time -= Time.deltaTime ;
        if(time < 0) gameObject.SetActive(false);
    }

    public void OnSpawn()
    {
        time = timeLife ;
        for(int i = 0 ; i < transform.childCount - 1 ; i++)
        {
            Rocket rocket = transform.GetChild(i).GetComponent<Rocket>();
            rocket.OnSpawn();
        } 
    }

    public void OnDespawn()
    {
        throw new System.NotImplementedException();
    }
}
