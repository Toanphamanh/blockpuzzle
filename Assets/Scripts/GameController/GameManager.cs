﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using DG.Tweening;
using System;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    public GameObject obj;
    [SerializeField] Grid[] slots;
    BlockDisplaySprite[] blockRepawns;
    public bool[] slotsCheck ;
    public int slotsCount ;
    public Vector2Int size, blockMaxSize;
    public GamePlayInfo currentInfo;
    public MyUnityEvent playerPickModeEvent;
    public UnityEventInt setBlockEvent;
    public PuzzleGame game ;
    public UnityEventGameStatus GamePlayStatusChangeEvent ;
    public GameNotice gameNotice ;
    public bool isNewRecord ;
    public bool isGamePlay , isInit , isPause , isDisplay , isRevive , isGameStart; 
    PersistableSO ps ;
    [SerializeField] GameSetting setting ;
    #region MonoBehavior
    void Awake()
    {
        if (instance == null) instance = this;
        else Destroy(this.gameObject);
        DOTween.useSmoothDeltaTime = true ;
        DOTween.SetTweensCapacity(500, 150) ;
        Application.targetFrameRate = 300;
        blockMaxSize = new Vector2Int(5, 5);
        playerPickModeEvent = new MyUnityEvent();
        playerPickModeEvent.AddListener(OnPlayerChoseMode);
        setBlockEvent = new UnityEventInt();
        setBlockEvent.AddListener(OnSetBlock);
        if(currentInfo == null)
        {
            //GamePlayInfo.lastInfo =  Resources.Load<GamePlayInfo>("DataSO/Timer");
            //currentInfo = GamePlayInfo.lastInfo;
        }
        if(GamePlayStatusChangeEvent == null)
        {
            GamePlayStatusChangeEvent = new UnityEventGameStatus();
            GamePlayStatusChangeEvent.AddListener(OnGamePlayStatusChange) ;
        }
         setting.OnStart();
         BlockInfo.Init();
         ps = GetComponent<PersistableSO>();
         ps.OnStart();
    }

    void OnEnable()
    {
       
    }
    void Start()
    {
        isInit = false ;
        isPause = false ;
        isGamePlay = false ;
        isDisplay = false ;
        isGameStart = false ;
        StartCoroutine(TimerCounter());
       // LoadGameData();
        
    }
    public void InitBlock(PlayModeType mode)
    {
        getGridSlot(mode);
    }

    void getGridSlot(PlayModeType mode)
    {
        slotsCheck = currentInfo.slotCheck;
        blockRepawns = new BlockDisplaySprite[3];
        slotsCount = 0;
        if(mode == PlayModeType.Classic8 || mode == PlayModeType.Classic10)
        {
            slots = UIManager.instance.classicBottomGrids ;
        }
        else
        {
            slots = UIManager.instance.bottomGrids;
        }
    }
    private void LoadSpawnPuzzle()
    {
        for (int i = 0; i < slots.Length; i++)
        {
            slotsCheck[i] = currentInfo.slotCheck[i];
            if(!slotsCheck[i]) slotsCount++ ;
            blockRepawns[i] = BlockRepawn(slots[i], i , currentInfo.spawnBlock[i]);
            blockRepawns[i].CheckSnap();
        }
        if(slotsCount == 3) slotsCount = 0;
    }


    IEnumerator TimerCounter()
    {
        yield return new WaitUntil(() => isGamePlay == true);
        
        while(currentInfo.timeLeft > 0)
        {
            
            yield return new WaitUntil(() => isGamePlay == true && currentInfo.playMode == PlayModeType.Timer) ;
            currentInfo.timeLeft -= Time.deltaTime;
            if(UIManager.instance != null) UIManager.instance.UpdateTimetxt();
            if(currentInfo.timeLeft < 0 && currentInfo.gameEnd == GameEnd.None)  
            {
                currentInfo.timeLeft = 0;
                currentInfo.gameEnd = GameEnd.TimeUp ;
                currentInfo.gameNotice = GameNotice.TimeisUp ;
                GamePlayStatusChangeEvent.Invoke(GameStatus.End);
                yield return new WaitUntil(() => currentInfo.timeLeft > 0) ;
            }
        }
        yield return null ;
    }
    void OnClassSpawnBlock()
    {
       // if(this.gameEnd != GameEnd.None) return ;
        Sequence s = DOTween.Sequence().SetAutoKill(false).Pause();
        int count = 0 ;
        float duration = 0.4f;
        for (int i = 0; i < slotsCheck.Length; i++)
        {
            if(!slotsCheck[i] && slotsCount != 0) continue ;
            blockRepawns[i].gameObject.layer = 2 ;
            
            if(count == 0 )
            {
                s.Append(blockRepawns[i].transform.DOMoveX(UIManager.instance.bottomGrids[i].transform.position.x , duration));
                count++ ;
            }  
            else s.Join(blockRepawns[i].transform.DOMoveX(UIManager.instance.bottomGrids[i].transform.position.x , duration));
        }
        s
         .AppendCallback(
            () =>
            {
                for(int i = 0 ; i < slotsCheck.Length ; i++)
                {
                    blockRepawns[i].gameObject.layer = 0;
                    if(!slotsCheck[i] && slotsCount != 0) continue;
                    blockRepawns[i].blockMoveRespawn();
                    slotsCheck[i] = true ;
                }
            }
        )

        .SetEase(Ease.InSine)
        .Restart();
    }
    #endregion
    #region  init player data
    #endregion
    #region events call back
    void OnPlayerChoseMode(PlayModeType mode)
    {
        isGamePlay = false ;
        isGameStart = false ;
        isRevive = true ;
        
        this.currentInfo = PersistableSO.instance.LoadDataSO(mode);
        if(mode == PlayModeType.Timer)
            UIManager.instance.turnOnTimerUI();
        else UIManager.instance.turnOffTimerUI();

       
        if(currentInfo.score > currentInfo.bestscore) isNewRecord = true ;
        else isNewRecord = false ;
        size = currentInfo.size;
        
        Debug.Log(size) ;
        if(game == null)
        {
            game = new PuzzleGame();
            MyGrid2D.instance.InitGame();
        }
        else if(MyGrid2D.gridSize != size) 
        {
             game = new PuzzleGame();
             //MyGrid2D.instance.StopAllCoroutines();
             MyGrid2D.instance.OnChangeCell();
             MyGrid2D.instance.InitGame();
        }

        MyGrid2D.instance.clearBoard();
        game.reset();
        game.LoadGameData();
        
        InitBlock(mode);
        

    }
    public void DisplayGame()
    {
        isDisplay = true;
    }
    public void StartCountTime()
    {
        StartCoroutine(TimerCounter());
    }
    public void GameStart()
    {
        LoadSpawnPuzzle();
        OnClassSpawnBlock();
        MyGrid2D.OnLoadCellInfo();
        
    }
    void OnSetBlock(int slotIndex)
    {
        slotsCount++;
        this.blockRepawns[slotIndex] = BlockRepawn(slots[slotIndex], slotIndex , null);
        // block
       
        if(currentInfo.playMode == PlayModeType.Bomb || currentInfo.playMode == PlayModeType.Timer )
        {
            slotsCheck[slotIndex] = true;
            CheckBlockMove();
        }
        else
        {
            slotsCheck[slotIndex] = false ;
            CheckBlockMoveClassic();
        } 
        if(Bomb.PlayerSetMoveEvent != null) Bomb.PlayerSetMoveEvent.Invoke();
       // SaveCurrentData();        
       // if(Equipment.OnPlayerClearEvent != null) Equipment.OnPlayerCheckClearEvent.Invoke(game.rowsToClear , game.colsToClear);
    }
    public void OnCheckBlockMove()
    {
        
    }
    void CheckBlockMove()
    {
        bool temp = true;
        for(int i = 0 ; i < slots.Length ; i++)
        {
            if(blockRepawns[i].isCanSnap)
            {
                temp = false ;
                break ;
            }
        }
        if(temp == true) 
        {
            OnNoMoveOccur();
        }
    }
    void CheckBlockMoveClassic()
    {
        bool temp = true;
        

        if(slotsCount == 3)
        {
            slotsCount = 0 ;
            OnClassSpawnBlock();
            for(int i = 0 ; i < slots.Length ; i++)
                if(blockRepawns[i].isCanSnap)
                {
                    temp = false ;
                    break ;
                }
        }
        else
        for(int i = 0 ; i < slots.Length ; i++)
        {
            if(slotsCheck[i] && blockRepawns[i].isCanSnap)
            {
                temp = false ;
                break ;
            }
        }
        if(temp == true) 
        {
            OnNoMoveOccur();
        }
    }
    void OnNoMoveOccur()
    {
        currentInfo.gameEnd = GameEnd.NoMove ;
        currentInfo.gameNotice = GameNotice.NoMove ;
        currentInfo.status = GameStatus.End ;
        GamePlayStatusChangeEvent.Invoke(GameStatus.End);
    }
    private void OnGamePlayStatusChange(GameStatus status)
    {
        //isGamePlay = false ;
        
        isGamePlay = false ;
        isGameStart = false ;
        PopUpManager.instance.isOnRevive = false ;
        if(status == GameStatus.Resume)
        {
            Time.timeScale = 1f;
            PopUpManager.instance.turnOnPauseBtn();
            currentInfo.status = GameStatus.Resume ;
        }
        if(status == GameStatus.Pause)
        {
            game.SaveGameData();
            UpdateCurrentGamePlayInfo();
            Time.timeScale = 0f  ;
        }
        if(status == GameStatus.Playing)
        {    
             currentInfo.status = GameStatus.Playing ;
             GameManager.instance.game.ClearEquiment();
             game.SaveGameData();
             UpdateCurrentGamePlayInfo();
        }
        if(status == GameStatus.Reset)
        {
            
            isPause = false ;
            currentInfo.status = GameStatus.Reset ;
            OnGameResetLevel();
            Time.timeScale = 1f;
            GameManager.instance.game.ClearEquiment();
            game.SaveGameData();
            UpdateCurrentGamePlayInfo();
            
        }
        if(status == GameStatus.End)
        {
            PopUpManager.instance.PopUpGameNotice()
            .AppendInterval(0.5f)
            .AppendCallback(()=>
             {
                OnGameEnd();
                game.SaveGameData();
                UpdateCurrentGamePlayInfo();
             }).Restart();
        }
    }

    public void SaveCurrentData()
    {
        if(game!=null) 
        {
            game.SaveGameData();
            UpdateCurrentGamePlayInfo();
        }
        
    }
    public void OnGameResetLevel()
    {
        currentInfo.ResetLevel();
        game.ClearEquiment();
        game.reset();
        resetPuzzle();
       // OnPlayerChoseMode(currentInfo.playMode);
        MyGrid2D.OnLoadCellInfo();
    }

    public void clearOnEnd()
    {
         game.ClearEquiment();
         clearSpawn();
         
    }
    void OnGameEnd()
    {
        if(currentInfo.gameEnd == GameEnd.NoMove && currentInfo.isCanRevive && ManagerAds.Ins.IsRewardVideoAvailable())
        {
            AudioManager.instance.turnOff("musicBg");
            AudioManager.instance.play("Revive");
            PopUpManager.instance.PopUpReviveUI();
            
        } 
        else 
        {
            PopUpManager.instance.OnReviveBtnNoClick();
        }
        
    }
    #endregion

    public BlockDisplaySprite BlockRepawn(Grid slot, int index , BlockInfo info)
    {
        if(info == null) info = BlockInfo.CreateRandomBlockInfo();
        
        GameObject ob = BlockPooler.Instance.RepawnBlock(info , slot , index) ;
        BlockDisplaySprite bdSprite = ob.GetComponent<BlockDisplaySprite>();
        bdSprite.OnSpawn();
        return bdSprite;
    }


    #region CallBackFunction
    public void resetPuzzle()
    {
        for (int i = 0; i < slots.Length; i++)
        {
            if (blockRepawns[i] != null)
            {
                blockRepawns[i].OnDespawn();
                
            }
             blockRepawns[i] = BlockRepawn(slots[i], i , null);
             slotsCheck[i] = true ;
        }
        if(currentInfo.playMode == PlayModeType.Classic8 || currentInfo.playMode == PlayModeType.Classic10)
        {
            OnClassSpawnBlock();
            slotsCount = 0 ;
        }    
        //slotsCount = 3 ;
    }
    #endregion

    public void clearSpawn()
    {
         for (int i = 0; i < slots.Length; i++)
        {
            if (blockRepawns[i] != null)
            {
                blockRepawns[i].OnDespawn();
            }
            blockRepawns[i] = null ;
        }
    }

    public void UpdateCurrentGamePlayInfo()
    {
        currentInfo.score = game.GetScore() ;
        if(currentInfo.score > currentInfo.bestscore)
        {
            isNewRecord = true;
            currentInfo.bestscore = currentInfo.score ;
        } 

        //currentInfo.bestscore = currentInfo.bestscore > currentInfo.score ? currentInfo.bestscore : currentInfo.score ;
        
        for(int i = 0 ; i < slots.Length ; i++)
        {
           if(blockRepawns[i] != null)
           {
               currentInfo.spawnBlock[i] = blockRepawns[i].info ;
               currentInfo.spawnBlock[i].data = blockRepawns[i].info.data ;
           } 
           currentInfo.slotCheck[i] = slotsCheck[i];
        }
        currentInfo.moveCount = game.moveCount ;
        
        PersistableSO.instance.SaveDataSO(GameManager.instance.currentInfo);
    }

    
    #region IEnumerator
  
    #endregion

    void OnApplicationQuit()
{
}
}

public enum GameStatus
{
    Resume ,
    Playing,
    Pause,
    Reset,
    End
}
public enum GameEnd
{
    None ,
    NoMove ,
    TimeUp ,
    BombBlast
}


public class MyUnityEvent : UnityEvent<PlayModeType>
{

}

public class UnityEventInt : UnityEvent<int>
{

}

public class UnityEventLong : UnityEvent<long>
{

}
public class UnityEventPlayerCheck : UnityEvent<List<int>, List<int>>
{

}
public class UnityEventVector2Int : UnityEvent<Vector2Int>
{

}

public class UnityEventBlockSnap : UnityEvent<BlockColor>
{

}

public class UnityEventGameStatus : UnityEvent<GameStatus>
{

}