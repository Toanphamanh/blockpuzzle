﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AudioManager : MonoBehaviour
{
    public static AudioManager instance ;
    [Header("Sounds Setting")]
    [Range(0f,2f)]
    public float musicVolume ;
    [Range(0f,2f)]
    public float soundVolume ;
    [Header("Sounds list")]
    [SerializeField]
    List<Sound> sounds ;
    [SerializeField]AudioSource bombEffect ;
    bool isMusicOn , isSoundOn ;
    float music , sound ;
    public static UnityEvent OnMusicVolumeChangeEvent , OnSoundVolumeChangeEvent ;
    void Awake()
    {   
        if(instance == null) instance = this ;
        foreach(Sound s in sounds)
        {
            s.source =  gameObject.AddComponent<AudioSource>() ;
            s.source.pitch = s.pitch ;
            s.source.volume = s.isSound ? s.volume * soundVolume : s.volume * musicVolume ;
            s.source.loop = s.isLoop ;
            s.source.clip = s.clip ;
            s.source.name = s.name ;
        }
        OnMusicVolumeChangeEvent = new UnityEvent();
        OnMusicVolumeChangeEvent.AddListener(OnMusicChange) ;
        OnSoundVolumeChangeEvent = new UnityEvent();
        OnSoundVolumeChangeEvent.AddListener(OnSoundChange) ;
        isSoundOn = isMusicOn = true ;
    }
    private void OnSoundChange()
    {
        sound = isSoundOn ? 0f : soundVolume ;
        isSoundOn = !isSoundOn ;
        ChangeMusic() ;
    }
    private void OnMusicChange()
    {
        music = isMusicOn ? 0f : soundVolume ;
        isMusicOn = !isMusicOn ;
        ChangeMusic() ;
    }

    void ChangeMusic()
    {
        foreach(Sound s in sounds)
        {
            s.source.volume = s.isSound ? s.volume * sound : s.volume * music ;
        }
    }
    // Update is called once per frame
    public void play(string name)
    {
        Sound s = sounds.Find(sound => sound.name == name);
        s.source.Play();
    }

    void playMusicBg()
    {
        play("BackGround");
        play("musicBg");
    }    
    public void play(string name , float time)
    {
        StartCoroutine(PlayLoop(name ,time)) ;
    }
     public void stop(string name)
    {
        Sound s = sounds.Find(sound => sound.name == name);
        s.source.Stop();
    }

    public void turnOff(string name)
    {
        Sound s = sounds.Find(sound => sound.name == name);
        s.source.volume = 0;
    }

    public void turnOn(string name)
    {
        Sound s = sounds.Find(sound => sound.name == name);
        s.source.volume = s.volume ;
    }
    void Start()
    {
        Invoke("playMusicBg" , 2f);
    }

    IEnumerator PlayLoop(string name , float time)
    {
        Sound s = sounds.Find(sound => sound.name == name);
        s.source.Play();
        while(time > 0)
        {
            time -= Time.deltaTime ;
            yield return new WaitForFixedUpdate() ;
        }
        s.source.Stop();
    }
    
}
[System.Serializable]
public class Sound
{
    public string name ;
    public AudioClip clip ;
    [Range(.1f,2f)]
    public float pitch ;
    [Range(0f,1f)]
    public float volume ;    
    public bool isLoop ;
    public bool isSound ;
    [HideInInspector]
    public AudioSource source ;
}
