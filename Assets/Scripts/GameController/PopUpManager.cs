﻿using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class PopUpManager : MonoBehaviour
{
    [SerializeField]
    GameObject ReviveUI, GameNoticeUI, ScoreUI, newRecordUI, topBoard, bottomBoard, menuSettingUI,
                                settingUI, recordUI, TimerBtn, Classic8Btn, BombBtn, Classic10Btn,
                                menuTop, menuLogo, menuCircle, mainBg, menuBg,
                                menuBottom, leaderBtn, rateBtn, shareBtn, settingBtn,
                                pauseBtn, soundOffImg, musicOffImg, soundOffMenuImg, musicOffMenuImg, tipUI, tipBtn, startGameImg;
    bool isShowVideo = false;
    public bool isDragBlock , isOnRevive ;
    public static PopUpManager instance;
    Vector3 beginTimerPos, beginBombPos, beginClassic8Pos;
    [SerializeField] float popUpTime;
    public List<Sprite[]> numberSpritesList;
    public Image pauseBtnImg, panelImg, menuPanelImg;
    Color panelColor;
    float startTime , lastClickTime;
    int pauseCount ;

    void Awake()
    {
        if (instance == null) instance = this;
        numberSpritesList = new List<Sprite[]>(6);
        for (int i = 0; i < 6; i++)
        {
            numberSpritesList.Add(new Sprite[10]);
            numberSpritesList[i] = Resources.LoadAll<Sprite>("ScoreFont/scorefont-export-" + i.ToString());
        }
        startTime = Time.time;
        pauseCount = 0 ;
        isDragBlock = false;
        isOnRevive = false ;
    }
    void Start()
    {
        panelColor = Color.black / 2;
        turnOffPanel();
        turnOffMenuPanel();
        pauseBtnImg = pauseBtn.GetComponent<Image>();
        isShowVideo = false;
        popUpTime = 0.35f;
        beginTimerPos = TimerBtn.transform.position;
        beginBombPos = BombBtn.transform.position;
        beginClassic8Pos = Classic8Btn.transform.position;
    }
    void Update()
    {

    }
    public Sequence PopUpSettingUI()
    {

        settingUI.transform.position = Vector3.up;
        settingUI.transform.localScale = Vector3.one * 0.15f;
        Sequence s = DOTween.Sequence().Pause().SetAutoKill(false);
        s.Append(settingUI.transform.DOScale(Vector3.one, popUpTime));

        return s;
    }

    public Sequence PopOutSettingUI()
    {
        Sequence s = DOTween.Sequence().Pause().SetAutoKill(false);
        turnOnPanel();
        Transform t = settingUI.transform;
        s.AppendCallback(turnOffPanel);

        s.Append(t.DOScale(new Vector3(1.1f, 1.1f, 1.1f), 0.1f))
            .Append(t.DOScale(Vector3.zero, 0.01f))
                .AppendCallback(() =>
                    {
                        turnOffPanel();
                        Debug.Log("PoPOutSetting");
                    });

        return s;
    }

    public Sequence PopUpStartGame()
    {
        Sequence s = DOTween.Sequence().SetAutoKill(false).Pause();
        Transform t = startGameImg.transform;
        t.localScale = Vector3.one * .8f;

        s.AppendCallback(() =>
        {
            if (!t.gameObject.activeSelf) t.gameObject.SetActive(true);
            AudioManager.instance.play("startGame");
        }
        )
         .Append(t.DOScale(1.5f, 0.4f))
         .Append(t.DOScale(1.2f, 0.6f))
         .AppendCallback(() =>
         {
             t.gameObject.SetActive(false);
             GameManager.instance.isGamePlay = true;
             GameManager.instance.isGameStart = true ;
             GameManager.instance.currentInfo.status = GameStatus.Playing ;
         }

          )
          .SetEase(Ease.InOutQuad);
        return s;
    }
    public void hideTipBtn()
    {
        tipBtn.SetActive(false);
    }

    public void showTipBtn()
    {
        tipBtn.SetActive(true);
    }
    public void turnOnTipBtn()
    {
        tipUI.GetComponent<Image>().raycastTarget = true;

    }
    public void turnOnPauseBtn()
    {
        pauseBtnImg.raycastTarget = true;
    }

    public void turnOffPauseBtn()
    {
        pauseBtnImg.raycastTarget = false;
    }
    public void turnOnPanel()
    {
        panelImg.raycastTarget = true;
        panelImg.color = panelColor;
    }
    public void turnOffPanel()
    {
        panelImg.raycastTarget = false;
        panelImg.color = Color.clear;
    }

    public void turnOnMenuPanel()
    {
        menuPanelImg.raycastTarget = true;
        menuPanelImg.color = panelColor;
    }

    public void turnOffMenuPanel()
    {
        menuPanelImg.raycastTarget = false;
        menuPanelImg.color = Color.clear;
    }

    public void PopUpReviveUI()
    {
        turnOnPanel();
        ReviveUI.transform.DOMoveY(0, popUpTime);
    }

    public Sequence PopOutReviveUI()
    {
        Sequence s = DOTween.Sequence().Pause().SetAutoKill(false);
        return s.Append(ReviveUI.transform.DOMoveY(20, popUpTime).OnComplete(() => turnOffPanel()));
    }

    public Sequence PopUpGameBoard()
    {
        Sequence s = DOTween.Sequence().Pause().SetAutoKill(false);
        s.AppendCallback(() =>
        {
            AudioManager.instance.play("Down");
            turnOffPauseBtn();
        })
            .Join(topBoard.transform.DOMove(Vector3.up * 1.2f, popUpTime))
            .Join(bottomBoard.transform.DOMove(Vector3.up * -3.2f, popUpTime))
                .AppendCallback(() =>
                {

                    GameManager.instance.DisplayGame();
                })
                .AppendInterval(1f)
                .AppendCallback(() =>
                {
                    GameManager.instance.GameStart();
                });
        if(GameManager.instance.currentInfo.gameEnd != GameEnd.None)
        {   
          
           s.AppendCallback(() => GameManager.instance.GamePlayStatusChangeEvent.Invoke(GameStatus.End));
          
        }
        else  s.Append(PopUpManager.instance.PopUpTip());
        return s;
    }



    public Sequence PopOutGameBoard()
    {
        Sequence s = DOTween.Sequence().Pause().SetAutoKill(false);

        s.AppendCallback(() =>
             {
                 GameManager.instance.clearOnEnd();
                 AudioManager.instance.play("Up");
             }

         )
            .Join(topBoard.transform.DOMoveY(15, popUpTime))
            .Join(bottomBoard.transform.DOMoveY(-15, popUpTime))
         ;
        return s;
    }

    public Sequence PopUpScore()
    {
        GameScoreUI sUI = ScoreUI.GetComponent<GameScoreUI>();
        Sequence s = DOTween.Sequence().Pause().SetAutoKill(false);
        sUI.ChangeIcon();
        sUI.reset();
        s.Append(ScoreUI.transform.DOMove(Vector3.zero, 0.1f))
            .Join(ScoreUI.transform.DOScale(Vector3.zero, 0.1f))
            .AppendCallback(() => AudioManager.instance.play("Over"))
            .Join(ScoreUI.transform.DOScale(Vector3.one * 1.1f, popUpTime))
                .Append(ScoreUI.transform.DOScale(Vector3.one, 0.25f))
                .AppendCallback(() => sUI.OnPopUp());


        return s;
    }

    public Sequence PopUpNewRecord()
    {
        Sequence s = DOTween.Sequence().Pause().SetAutoKill(false);
        s.Append(recordUI.transform.DOMoveY(0, popUpTime))
            .AppendCallback(() =>
            {
                RecordUI rcUI = recordUI.GetComponent<RecordUI>();
                rcUI.anim.SetTrigger("playTrigger");
                rcUI.OnAppear();
                AudioManager.instance.play("NewBest");
            });

        return s;
    }


    public void OnReCordCloseBtnClick()
    {

        Sequence s = DOTween.Sequence().Pause().SetAutoKill(false);
        s.Append(recordUI.transform.DOMoveY(-10f, popUpTime))
          .Append(PopUpScore())
           .AppendCallback(() =>
           {
               RecordUI rcUI = recordUI.GetComponent<RecordUI>();
               rcUI.anim.SetTrigger("playTrigger");
           })
           .Restart();
    }
    public Sequence PopOutScore()
    {
        Sequence s = DOTween.Sequence().Pause().SetAutoKill(false);
        s.Append(ScoreUI.transform.DOScale(Vector3.zero, popUpTime));
        return s;
    }

    public Sequence PopUpMenu()
    {
        Sequence s = DOTween.Sequence().SetAutoKill(false).Pause();
        s.Append(mainBg.transform.GetComponent<Image>().DOFade(1f, popUpTime))
                         .Append(menuTop.transform.DOScale(Vector3.one * 1.1f, popUpTime))
                         .Join(menuBg.transform.DOMoveX(0, popUpTime))
                         .Join(menuBottom.transform.DOScale(Vector3.one * 1.1f, popUpTime))
                         .Join(Classic10Btn.transform.DOScale(Vector3.one * 1.1f, popUpTime))
                            .Append(Classic10Btn.transform.DOScale(Vector3.one, popUpTime / 2))
                            .Join(menuBottom.transform.DOScale(Vector3.one, popUpTime / 2))
                            .Join(menuTop.transform.DOScale(Vector3.one, popUpTime / 2))
                                .Join(TimerBtn.transform.DOMove(beginClassic8Pos, popUpTime)) //
                                .Join(BombBtn.transform.DOMove(beginClassic8Pos, popUpTime))
                                .Join(Classic8Btn.transform.DOMove(beginClassic8Pos, popUpTime))
                                .Join(Classic8Btn.transform.DOScale(Vector3.one, popUpTime))
                                .Join(TimerBtn.transform.DOScale(Vector3.one, popUpTime))
                                .Join(BombBtn.transform.DOScale(Vector3.one, popUpTime))
                                    .Append(BombBtn.transform.DOMove(beginBombPos, popUpTime))
                                    .Join(TimerBtn.transform.DOMove(beginTimerPos, popUpTime)).SetEase(Ease.InOutQuad);
        return s;
    }

    public Sequence PopOutMenu()
    {
        Sequence s = DOTween.Sequence().SetAutoKill(false).Pause();
        s.Append(TimerBtn.transform.DOMove(Classic8Btn.transform.position, popUpTime))
                          .Join(BombBtn.transform.DOMove(Classic8Btn.transform.position, popUpTime))
                            .Append(BombBtn.transform.DOScale(Vector3.zero, popUpTime))
                            .Join(TimerBtn.transform.DOScale(Vector3.zero, popUpTime))
                            .Join(Classic8Btn.transform.DOScale(Vector3.zero, popUpTime))
                            .Join(Classic8Btn.transform.DOMove(Classic10Btn.transform.position, popUpTime))
                            .Join(BombBtn.transform.DOMove(Classic10Btn.transform.position, popUpTime))
                            .Join(TimerBtn.transform.DOMove(Classic10Btn.transform.position, popUpTime))
                                .Append(menuTop.transform.DOScale(Vector3.one * 1.1f, popUpTime / 2))
                                .Join(menuBottom.transform.DOScale(Vector3.one * 1.1f, popUpTime / 2))
                                .Join(Classic10Btn.transform.DOScale(Vector3.one * 1.1f, popUpTime / 2))
                                    .Append(Classic10Btn.transform.DOScale(Vector3.zero, popUpTime))
                                    .Join(menuBottom.transform.DOScale(Vector3.zero, popUpTime))
                                    .Join(menuBg.transform.DOMoveX(15, popUpTime))
                                    .Join(menuTop.transform.DOScale(Vector3.zero, popUpTime))
                                        .Append(mainBg.transform.GetComponent<Image>().DOFade(0f, popUpTime));
        return s;
    }

    void menuButtonPopOut()
    {

    }
    public Sequence PopUpGameNotice()
    {
        Sequence s = DOTween.Sequence().Pause().SetAutoKill(false);
        GameNoticeUI.GetComponent<GameNoticeUI>().Display(GameManager.instance.currentInfo.gameNotice);
        AudioManager.instance.play("GameLose");
        s.Append(GameNoticeUI.transform.FadeIn(0.5f));
        return s;
    }

    public Sequence PopUpTip()
    {
        Sequence s = DOTween.Sequence().Pause().SetAutoKill(false);
        s.AppendCallback(() => tipUI.GetComponent<TipUI>().Display());
        return s;
    }
    #region Button CallBack
    public void OnReviveBtnNoClick()
    {
        //API.Instance.ShowFull(OnEnd);
        ManagerAds.Ins.ShowInterstitial(OnEnd);
    }

    private Sequence PopOutGameNotice()
    {
        Sequence s = DOTween.Sequence().Pause().SetAutoKill(false);
        s.Append(GameNoticeUI.transform.FadeOut(0.4f)).Restart();
        return s;
    }


    public Sequence OnEndGameWithNoRevive()
    {
        Sequence s = DOTween.Sequence().Pause().SetAutoKill(false);

        s.AppendCallback(() =>
        {
           // GameManager.instance.game.SaveGameData();
           // GameManager.instance.UpdateCurrentGamePlayInfo();

            UIManager.GameEndEvent.Invoke();
        })
                .AppendInterval(2f)

                    .Append(PopOutGameBoard())
                    .Join(PopOutGameNotice())
                        .AppendCallback(() =>
                        {
                            turnOffPanel();
                        }
                        );
        if (GameManager.instance.isNewRecord)
        {
            s.Append(PopUpNewRecord());
            GameManager.instance.isNewRecord = false;
        }
        else s.Append(PopUpScore());
        return s;

    }
    public void OnReviveBtnYesClick()
    {
        AudioManager.instance.play("BtnClick");
        AudioManager.instance.stop("Revive");
        AudioManager.instance.turnOn("musicBg");
        isOnRevive = true ;
        OnShowRewardedBasedVideo();
    }

    List<Vector2Int> getRandom()
    {
        Vector2Int size = GameManager.instance.size;
        GridCellUI c;
        List<Vector2Int> list = new List<Vector2Int>(100);
        for (int i = 0; i < size.x; i++)
            for (int j = 0; j < size.y; j++)
            {
                c = MyGrid2D.cellUIs[i, j];
                if (c.value != 0)
                {
                    list.Add(new Vector2Int(i, j));
                    Debug.Log(string.Format("{0} + {1}", i, j));
                }
            }
        int n = (int)(list.Count * 1f / 3), r;

        for (int i = 0; i < n; i++)
        {
            r = UnityEngine.Random.Range(0, list.Count);
            list.RemoveAt(r);
        }
        return list;
    }

    Sequence DoShake(List<Vector2Int> list)
    {
        Sequence s = DOTween.Sequence().Pause();
        GridCellUI c;
        for (int i = 0; i < list.Count; i++)
        {
            c = MyGrid2D.cellUIs[list[i].x, list[i].y];
            if (i == 0) s.Append(c.blockObject.transform.DOShakePosition(2f, 10f, 10, 30));
            else s.Join(c.blockObject.transform.DOShakePosition(2f, 10f, 10, 30));
        }

        return s;
    }

    Sequence DoFade(List<Vector2Int> list)
    {

        Sequence s = DOTween.Sequence().Pause().SetAutoKill(false);
        GridCellUI c;
        for (int i = 0; i < list.Count; i++)
        {
            c = MyGrid2D.cellUIs[list[i].x, list[i].y];
            if (i == 0) s.Append(c.blockImg.DOFade(0f, 1f));
            else s.Join(c.blockImg.DOFade(0f, 1f));
            c.value = 0;
            GameManager.instance.game.gameData[list[i].x, list[i].y] = 0;
            GameManager.instance.game.tempData[list[i].x, list[i].y] = 0;
            GameManager.instance.currentInfo.gameData[list[i].x * GameManager.instance.size.y + list[i].y].type = EquipmentType.None ;
     
            GameManager.instance.game.equipmentSpaceList.Remove(list[i]);
            
            
            if (c.equipObject != null)
            {
                StopWatch sw = c.equipObject.GetComponent<StopWatch>();
                if(sw != null) sw.BeginFly();
                else        
                c.equipObject.SetActive(false);
                c.equipObject = null;
            }
            if (GameManager.instance.game.rocketList.Contains(list[i]))
            {
                GameManager.instance.game.rocketList.Remove(list[i]);
            }
        }

        

        return s;
    }
    public void OnRevive()
    {
        Sequence s = DOTween.Sequence().Pause().SetAutoKill(false);
        List<Vector2Int> list = getRandom();


        s.Append(PopOutGameNotice()).Append(PopOutReviveUI()).Append(DoShake(list)).Append(DoFade(list))
         .AppendCallback(() =>
          {
              GameManager.instance.resetPuzzle();
              GameManager.instance.currentInfo.gameEnd  = GameEnd.None ;
              GameManager.instance.currentInfo.status = GameStatus.Playing ;
              GameManager.instance.currentInfo.isCanRevive = false ;
              PopUpManager.instance.isOnRevive = false ;
              GameManager.instance.isGamePlay = true;
            
              GameManager.instance.UpdateCurrentGamePlayInfo();
          }

        )
        .Restart();

    }
    Sequence ResumeGame()
    {
        Sequence s = DOTween.Sequence().Pause().SetAutoKill(false);
        s.AppendCallback(() => GameManager.instance.GamePlayStatusChangeEvent.Invoke(GameStatus.Resume));
        Debug.Log("Resume");
        return s;
    }
    Sequence PauseGame()
    {
        Sequence s = DOTween.Sequence().Pause().SetAutoKill(false);
        s.AppendCallback(() => GameManager.instance.GamePlayStatusChangeEvent.Invoke(GameStatus.Pause));
        Debug.Log("Pause");

        return s;
    }

    public void OnPauseBtnClick()
    {
        if(isDragBlock) return;
        if(Time.time - lastClickTime < 1 && pauseCount > 0) return ;

        pauseCount ++ ;
        lastClickTime = Time.time ;

        if (Time.time - startTime < 15)
        {
            OnPause();
            return;
        }
        else if (pauseCount == 2 || Time.time - startTime > 120)
        {
            ManagerAds.Ins.ShowInterstitial(() =>
            {
                OnPause();
                startTime = Time.time;
            });
            //API.IsAdsClick = false;
            pauseCount = 0 ;
        }
        else
            OnPause();
    }


    public void OnSettingCloseBtnClick()
    {
        AudioManager.instance.play("BtnClick");
        ResumeGame().Append(PopOutSettingUI())
        .AppendCallback(() =>
        {
            GameManager.instance.isGamePlay = true;
            pauseBtn.GetComponent<Image>().raycastTarget = true;
        }).Restart();
    }

    public void OnTryAgainBtnClick()
    {
        AudioManager.instance.play("BtnClick");
        GameManager.instance.GamePlayStatusChangeEvent.Invoke(GameStatus.Reset);
        //GameManager.instance.playerPickModeEvent.Invoke(GameManager.instance.currentInfo.playMode) ;
        PopOutScore()
            .AppendCallback(
                () =>
            {
                MyGrid2D.instance.clearBoard();
            }
            )
            .Append(UIManager.instance.setUpGame())
            
            .Restart();
    }

    public void OnReTryBtnClick()
    {
        AudioManager.instance.play("BtnClick");
        GameManager.instance.GamePlayStatusChangeEvent.Invoke(GameStatus.Reset);
        ResumeGame().Append
        (PopOutSettingUI())
            .AppendCallback(() => MyGrid2D.instance.isResetGame = true)
            .AppendInterval(1.5f)
            .AppendCallback(() => GameManager.instance.isDisplay = true)
            .AppendInterval(1f)
            .AppendCallback(() =>
            {
                UIManager.instance.UpdateScoreEvent.Invoke(0);
                pauseBtn.GetComponent<Image>().raycastTarget = true;
                
            })
            .Append(PopUpStartGame()).Restart();
    }

    public void OnExitSettingBtnClick()
    {
        AudioManager.instance.play("BtnClick");
        GameManager.instance.clearSpawn();
        GameManager.instance.GamePlayStatusChangeEvent.Invoke(GameStatus.Playing);
        ResumeGame()
         .Append(PopOutSettingUI())
            .Append(PopUpMenu())
            .AppendCallback(() =>
            {
                pauseBtn.GetComponent<Image>().raycastTarget = true;
            }).Restart();
        Debug.Log("ExitSetting");
    }

    public void OnExitScoreBtnClick()
    {
        GameManager.instance.GamePlayStatusChangeEvent.Invoke(GameStatus.Reset);
        AudioManager.instance.play("BtnClick");
        PopOutScore()
                .Append(PopUpMenu())
                .AppendCallback(() =>
                {
                    
                })
            .Restart();
    }

    public void OnSoundToggleClick()
    {
        AudioManager.instance.play("BtnClick");
        bool state = soundOffImg.activeSelf;
        soundOffImg.SetActive(!state);
        soundOffMenuImg.SetActive(!state);

        AudioManager.OnSoundVolumeChangeEvent.Invoke();
    }

    public void OnMusicToggleClick()
    {
        AudioManager.instance.play("BtnClick");
        bool state = musicOffImg.activeSelf;
        musicOffImg.SetActive(!state);
        musicOffMenuImg.SetActive(!state);

        AudioManager.OnMusicVolumeChangeEvent.Invoke();
    }

    public void OnSoundMenuTogleClick()
    {
        AudioManager.instance.play("BtnClick");
        bool state = soundOffMenuImg.activeSelf;
        soundOffMenuImg.SetActive(!state);
        soundOffImg.SetActive(!state);

        AudioManager.OnSoundVolumeChangeEvent.Invoke();
    }

    public void OnMusicMenuToggleClick()
    {
        AudioManager.instance.play("BtnClick");
        bool state = musicOffMenuImg.activeSelf;
        musicOffMenuImg.SetActive(!state);
        musicOffImg.SetActive(!state);

        AudioManager.OnMusicVolumeChangeEvent.Invoke();
    }
    public void OnTipBtnClick()
    {
        PopUpTip().Restart();
    }


    #endregion
    #region  API Ads
    public void OnRateUsBtnClick()
    {
        ManagerAds.Ins.RateApp();
    }

    public void OnShareBtnClick()
    {
        ManagerAds.Ins.ShareApp();
    }

    public void OnShowRewardedBasedVideo()
    {
        ManagerAds.Ins.ShowRewardedVideo((_) =>
        {
            if(_)
            {
                OnRewarded();
            }
        });
    }

    public void OnMenuSettingBtnClick()
    {
        turnOnMenuPanel();
        menuSettingUI.transform.DOScale(Vector3.zero, 0.1f);
        menuSettingUI.transform.DOMove(Vector3.zero, 0.1f);
        menuSettingUI.transform.DOScale(Vector3.one, popUpTime);
    }

    public void OnQuitBtnClick()
    {
        Application.Quit();
    }


    public void OnCloseMenuSettingBtnClick()
    {
        menuSettingUI.transform.DOScale(Vector3.zero, popUpTime);
        turnOffMenuPanel();
    }
    private void OnRewarded()
    {
        OnRevive();
    }

    void OnPause()
    {
        AudioManager.instance.play("BtnClick");
        pauseBtn.GetComponent<Image>().raycastTarget = false;
        PopUpSettingUI().Append(PauseGame()).Restart();
        turnOnPanel();
    }

    void OnEnd()
    {
        AudioManager.instance.stop("Revive");
        AudioManager.instance.turnOn("musicBg");
        AudioManager.instance.play("BtnClick");

        GameNoticeUI.GetComponent<GameNoticeUI>().Display(GameManager.instance.currentInfo.gameNotice);
        Sequence s = DOTween.Sequence().Pause().SetAutoKill(false);
        ManagerAds.Ins.ShowInterstitial(
            () => s.Append(PopOutReviveUI())
            .Append(OnEndGameWithNoRevive()).Restart()
        );
    }
    #endregion
}
