﻿using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.Events;

public class UIManager : MonoBehaviour
{
    #region field 
    public static UIManager instance;
    public Sprite gridcellSprite, hintCellSprite;
    [HideInInspector]
    public Sprite[] blockSprites;
    public Grid[] bottomGrids , classicBottomGrids;
    public Transform blocksT, dragBlocksT, blockMoveT, midGameBgT, bottomGameBgT, equipmentT, snapObjectT, scoreT , SpwanTimeBonusPos;
    //public GameObject blockObject ;
    public Vector2  blockBottomSize;
    public GameObject clearBlockPref, blockCellObject, rocketLanchPref;
    [SerializeField] GameObject menuPanel, gameBgPanel, gamePlayPanel, midGameBg, bottomGameBg ; 
    [SerializeField] Image classic10Btn , classic8Btn , timeBtn , bombBtn ;
    [SerializeField] Transform midGameBgPos, bottomGameBgPos;
    [SerializeField] Text  timerTxt;
    bool isRockketClear;
    public GameObject[] equipmentObjects;
    public UnityEventInt UpdateScoreEvent;
    [SerializeField] GameObject timerUI  ;
    [SerializeField] Image timerFill;
    public RectTransform classSpawnBlocksT;
    public Camera mainCamera ;
    public Transform timerIcon  ;

    public static UnityEventVector2Int SpawnTimerEvent, SpawnRocketEvent, SpawnBombEvent;
    public static UnityEventInt TimeBonusEvent ;
    public static UnityEvent GameEndEvent  ;
    [Header("Cell UI")]
    public Sprite[] endCellSprites;
    [HideInInspector]
    public Sprite endCellSprite;
    [Header("Score Sprites")]
    public Sprite[] scoreSprites;
    public TextNumberHelper score , highScore ;

    Sequence closeMenu ;
    // Use this for initialization
    #endregion
    void Awake()
    {
        if (instance == null)
            instance = this;
        else
            Destroy(gameObject);
        blockSprites = Resources.LoadAll<Sprite>("Sprites/BlockSprites");
    }
    void Start()
    {
        blockBottomSize = bottomGrids[0].cellSize;
        if (blockCellObject == null)
        {
            blockCellObject = new GameObject("BlockCell");
        }
        Init();
        UpdateScoreEvent = new UnityEventInt();
        UpdateScoreEvent.AddListener(OnUpdateScore);
        SpawnTimerEvent = new UnityEventVector2Int();
        SpawnTimerEvent.AddListener(OnSpawnTimer);
        SpawnBombEvent = new UnityEventVector2Int();
        SpawnBombEvent.AddListener(OnSpawnBomb);
        SpawnRocketEvent = new UnityEventVector2Int();
        SpawnRocketEvent.AddListener(OnSpawRocket);
        GameEndEvent = new UnityEvent();
        GameEndEvent.AddListener(OnGameEndEvent);
        TimeBonusEvent = new UnityEventInt();
        TimeBonusEvent.AddListener(OnTimeBonus) ;

        

    }
    // Update is called once per frame
    public void UpdateTimetxt()
    {
        if(!GameManager.instance.isGamePlay) return ;
        float time = GameManager.instance.currentInfo.timeLeft;
        timerFill.fillAmount = time / GameManager.instance.currentInfo.maxTime;
        string s = string.Format("{0}:{1:00}", (int)time / 60, (int)(time % 60));
        timerTxt.text = s;
    }
    public void startTimeTxt()
    {
        timerFill.fillAmount = 1 ;
        timerTxt.text = "2:00";
    }
    void Init()
    {
        midGameBg.transform.position = Vector3.up * 10f;
        bottomGameBg.transform.DOMoveY(-10f, 0f);
        resetScoreUI();
    }

    public void turnOnTimerUI()
    {
        timerUI.SetActive(true);
    }

    public void turnOffTimerUI()
    {
        timerUI.SetActive(false);
    }

    void turnOffBtnMenu()
    {
        classic10Btn.raycastTarget = false ;
        classic8Btn.raycastTarget = false ;
        timeBtn.raycastTarget = false ;
        bombBtn.raycastTarget = false ;
    }

    void turnOnBtnMenu()
    {
        classic10Btn.raycastTarget = true ;
        classic8Btn.raycastTarget = true ;
        timeBtn.raycastTarget = true ;
        bombBtn.raycastTarget = true ;
    }


    public Sequence setUpGame()
    {
        highScore.value = 0 ;
        highScore.Init();
        highScore.Display() ;
        Sequence s = DOTween.Sequence().SetAutoKill(false).Pause();
        s.
             Append(PopUpManager.instance.PopUpGameBoard())
            .AppendCallback(() =>
        {
           if(GameManager.instance.currentInfo.status == GameStatus.Playing)
           {
               score.value = 0;
               score.Init();
               score.Display();
               score.RollNumber(GameManager.instance.currentInfo.score , 1f) ;
           }
           else
           {
               score.value = GameManager.instance.currentInfo.lastScore ;
               score.Init();
               score.Display();
               score.RollNumber(0 , 1f) ;
           }
           highScore.RollNumber(GameManager.instance.currentInfo.bestscore , 1f);
        })        
        .Restart();
        return s;
    }

    public void UpdateBestScoreTxt()
    {
        highScore.RollNumber(GameManager.instance.currentInfo.bestscore , 1f);
    }
    #region Button Function
    public void OnButtonClassic10Click()
    {
        turnOffBtnMenu();
        PopUpManager.instance.hideTipBtn();
        GameManager.instance.playerPickModeEvent.Invoke(PlayModeType.Classic10);
        AudioManager.instance.play("BtnClick");
        CloseMenuPanel().AppendCallback(() =>
        {
            classic10Btn.raycastTarget = true ;
            turnOnBtnMenu();
            turnOffTimerUI();
        }).Restart();
    }
    public void OnButtonClassic8Click()
    {
        turnOffBtnMenu();
        PopUpManager.instance.hideTipBtn();
        GameManager.instance.playerPickModeEvent.Invoke(PlayModeType.Classic8);
        AudioManager.instance.play("BtnClick");
        CloseMenuPanel().AppendCallback(() => 
        {
             classic8Btn.raycastTarget = true;
             turnOnBtnMenu();
             turnOffTimerUI();
        }).Restart();
       
    }

    public void OnButtonTimerClick()
    {
        turnOffBtnMenu();
        PopUpManager.instance.showTipBtn();
        GameManager.instance.playerPickModeEvent.Invoke(PlayModeType.Timer);
        AudioManager.instance.play("BtnClick");
        CloseMenuPanel().AppendCallback(() => 
        {
            timeBtn.raycastTarget = true ;
            turnOnBtnMenu();
            turnOnTimerUI();
        }
        ).Restart();
        
    }

    public void OnButtonBombClick()
    {
        turnOffBtnMenu();
        PopUpManager.instance.showTipBtn();
        timerUI.SetActive(false);
        GameManager.instance.playerPickModeEvent.Invoke(PlayModeType.Bomb);
        AudioManager.instance.play("BtnClick");
        CloseMenuPanel().AppendCallback( () =>
        {
            bombBtn.raycastTarget = true ;
            turnOnBtnMenu();
            turnOffTimerUI();
        })
        .Restart();
    }
    Sequence CloseMenuPanel()
    {
        closeMenu = DOTween.Sequence().SetAutoKill(false).Pause();
        closeMenu.AppendInterval(0.2f)
                //.AppendCallback(
                    //() => resetScoreUI()
               // )
                .Append(PopUpManager.instance.PopOutMenu())
                
                .Append(setUpGame());
        return closeMenu ;
    }

    public void resetScoreUI()
    {
        score.value = 0 ;
        score.Init();
        score.Display();
        highScore.value = 0 ;
        highScore.Init() ;
        highScore.Display();
    }

    public void resetTimeUI()
    {
    }    
    #endregion

    #region  Events Callback
    void OnUpdateScore(int scoreValue)
    {
        score.RollNumber(scoreValue , 0.5f);
        GameManager.instance.currentInfo.lastScore = scoreValue ;
    }
    void OnSpawnTimer(Vector2Int pos)
    {
        GameObject ob = ObjectPooler.instance.SpawnFromPool("Timer", MyGrid2D.cellUIs[pos.x, pos.y].transform.position);
        StopWatch time = ob.GetComponent<StopWatch>();
        time.pos = pos;
        time.setTime(20);
        time.OnSpawn();
        
        MyGrid2D.cellUIs[pos.x, pos.y].equipObject = ob;
        MyGrid2D.cellUIs[pos.x, pos.y].cellInfo.type = EquipmentType.Timer;
    }

    void OnSpawRocket(Vector2Int pos)
    {
        GameObject ob = ObjectPooler.instance.SpawnFromPool("RocketIdle", MyGrid2D.cellUIs[pos.x, pos.y].transform.position);
        RocketIdle ri = ob.GetComponent<RocketIdle>();
        ri.pos = pos;
        ri.OnSpawn();
        MyGrid2D.cellUIs[pos.x, pos.y].equipObject = ob;
        MyGrid2D.cellUIs[pos.x, pos.y].cellInfo.type = EquipmentType.Rocket;
    }
    void OnSpawnBomb(Vector2Int pos)
    {
        GameObject ob = ObjectPooler.instance.SpawnFromPool("Bomb", MyGrid2D.cellUIs[pos.x, pos.y].transform.position);
        Bomb bomb = ob.GetComponent<Bomb>();
        bomb.pos = pos;
        bomb.setMoveCount(10);
        bomb.OnSpawn();
        MyGrid2D.cellUIs[pos.x, pos.y].equipObject = ob;
        MyGrid2D.cellUIs[pos.x, pos.y].cellInfo.type = EquipmentType.Bomb;

    }

    public void OnPauseBtnClick()
    {
        AudioManager.instance.play("BtnClick");
        GameManager.instance.GamePlayStatusChangeEvent.Invoke(GameStatus.Pause);

    }

    private void OnGameEndEvent()
    {
        PlayModeType type = GameManager.instance.currentInfo.playMode;
        int index = (int)type;
        if (index == 0) index = 0;
        else index = index - 1;
        endCellSprite = endCellSprites[index];
        GridCellUI.ChangeCellSpriteEvent.Invoke();
    }

    private void OnTimeBonus(int time)
    {
        GameObject timeBonus = ObjectPooler.instance.SpawnFromPool("TimeBonus" , SpwanTimeBonusPos.position);
        IPooledObject pO = timeBonus.GetComponent<IPooledObject>();
        TimeBonusMove tm = timeBonus.GetComponent<TimeBonusMove>();
        tm.bonusTime = time;
        
        pO.OnSpawn();
    }
    void OnGameReset()
    {

    }
    #endregion

    #region Coroutine

    #endregion
}
