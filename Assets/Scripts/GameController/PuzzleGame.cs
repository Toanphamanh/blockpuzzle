﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PuzzleGame
{
    public int[,] gameData, tempData;
    int score = 0;
    public int moveCount = 0;
    static int bombStep = 5;
    Vector2Int size;
    public Vector3 scorePos;
    public static int[] scoreValues = { 72, 216, 432, 864, 1728, 3456 };
    public List<int> rows, cols, rowsToScore, colsToScore, rowsToClear, colsToClear   ;
    GamePlayInfo currentPlayInfo;
    public List<Vector2Int> equipmentSpaceList, rocketList, rocketFireList, bombList , setBlockPos;
    PlayModeType mode;
    bool isSpawmEquipment;
    static int maxBomb = 5 , maxRocket = 8;
    public static BlockColor scoreColor ;
    #region Init Game Data
    public PuzzleGame()
    {
        currentPlayInfo = GameManager.instance.currentInfo;
        this.size = GameManager.instance.currentInfo.size;
        gameData = new int[size.x, size.y];
        tempData = new int[size.x, size.y];
        rows = new List<int>(5);
        cols = new List<int>(5);
        
        rowsToClear = new List<int>(size.y);
        colsToClear = new List<int>(size.x);
        rowsToScore = new List<int>(5);
        colsToScore = new List<int>(5);

        equipmentSpaceList = new List<Vector2Int>(size.x * size.y);
        rocketList = new List<Vector2Int>(size.x * size.y);
        setBlockPos = new List<Vector2Int>(25);
        bombList = new List<Vector2Int>(maxBomb);
        // equipmentList = new List<Vector2Int>(size.x * size.y) ;
        rocketFireList = new List<Vector2Int>(rocketList.Count);
        
        mode = GameManager.instance.currentInfo.playMode;
        Init();
       // LoadGameData();
        if (GameManager.instance.currentInfo.status == GameStatus.Reset)
        {
            GameManager.instance.currentInfo.gameEnd = GameEnd.None;
        }
        else if (GameManager.instance.currentInfo.status == GameStatus.Playing)
        {
            score = currentPlayInfo.score;
            moveCount = currentPlayInfo.moveCount;
        }
        if (GameManager.instance.currentInfo.status == GameStatus.End)
        {
           // GameManager.instance.currentInfo.status = GameStatus.Playing ;
           // GameManager.instance.currentInfo.gameEnd = GameEnd.None ;
        }
        //Mode = currentPlayInfo.playMode ;
    }
    public void reset()
    {
        Init() ;
          
        for (int i = 0; i < size.x; i++)
            for (int j = 0; j < size.y; j++)
            {
                gameData[i, j] = 0;
                tempData[i, j] = 0;
            }
    }
    public void LoadGameData()
    {
        mode = GameManager.instance.currentInfo.playMode;
        for (int i = 0; i < size.x; i++)
            for (int j = 0; j < size.y; j++)
            {
                int cell = (int)GameManager.instance.currentInfo.gameData[i * size.x + j].type;
                gameData[i, j] = cell;
                tempData[i, j] = cell;
                if (cell == 1)
                {
                    equipmentSpaceList.Add(new Vector2Int(i, j));
                }
                if (cell == 3)
                {
                    rocketList.Add(new Vector2Int(i, j));
                    //Debug.Log("Rocket at" + i.ToString() + " " + j.ToString());
                }
                if (cell > 1)
                {
                }
            }
        score = GameManager.instance.currentInfo.score ;
        moveCount = GameManager.instance.currentInfo.moveCount;
       // ReadGameData();
    }
    public void SaveGameData()
    {
        for (int i = 0; i < size.x; i++)
            for (int j = 0; j < size.y; j++)
            {
                int cell = gameData[i, j];
                
                EquipmentType equip = (EquipmentType)cell;
                GameManager.instance.currentInfo.gameData[i * size.x + j].type = equip ;
                GridCellUI cellUI  = MyGrid2D.cellUIs[i,j];
                if(cellUI.equipObject == null) continue ;
                if(equip == EquipmentType.Timer)
                {
                    StopWatch sw = cellUI.equipObject.GetComponent<StopWatch>() ;
                    if(sw!=null && sw.gameObject.activeInHierarchy)
                    {
                        GameManager.instance.currentInfo.gameData[i*size.x + j].counter = sw.getTime();
                    } 
                }
                else if(equip == EquipmentType.Bomb)
                {
                    Bomb bomb = cellUI.equipObject.GetComponent<Bomb>();
                    if(bomb != null && bomb.gameObject.activeInHierarchy)
                        GameManager.instance.currentInfo.gameData[i*size.x + j].counter = bomb.getMoveCount();
                }
            }
            //PersistableSO.instance.SaveDataSO(GameManager.instance.currentInfo);
    }
    void Init()
    {
        score = 0;
        moveCount = 0;
        equipmentSpaceList.Clear();
        rocketFireList.Clear();
        rocketList.Clear();
        if (mode == PlayModeType.Bomb) bombList.Clear();
        rows.Clear();
        cols.Clear();
        rowsToScore.Clear();
        colsToScore.Clear();
        isSpawmEquipment = false;

    }
    #endregion
    #region Game Play Core
    public int getCol(int col)
    {
        int rs = 0;
        for (int iRow = 0; iRow < size.x; iRow++)
            if (tempData[col, iRow] != 0) rs++;
        return rs;
    }
    public int getRow(int row)
    {
        int rs = 0;
        for (int iCol = 0; iCol < size.y; iCol++)
            if (tempData[iCol, row] != 0) rs++;
        return rs;
    }
    public void clearCol(int col, int value, bool isTry)
    {
        for (int iRow = 0; iRow < size.x; iRow++)
        {
            if (!isTry) gameData[col, iRow] = value;
            tempData[col, iRow] = value;
        }

    }
    public void clearRow(int row, int value, bool isTry)
    {
        for (int iCol = 0; iCol < size.y; iCol++)
        {
            if (!isTry) gameData[iCol, row] = value;
            tempData[iCol, row] = value;
        }
    }
    public void AddListRowCheck(int value)
    {
        if (!rows.Contains(value))
        {
            rows.Add(value);
        }
    }
    public void AddListColCheck(int value)
    {
        if (!cols.Contains(value))
        {
            cols.Add(value);
        }
    }
    public void ReadGameData()
    {
        string[] s = new string[size.x],
        s1 = new string[size.x];
        for (int j = size.y - 1; j >= 0; j--)
        {
            for (int i = 0; i < size.x; i++)
            {
                s[j] += gameData[i, j] + "  ";
                s1[j] += tempData[i, j] + "  ";
            }
        }
        for (int j = size.y - 1; j >= 0; j--)
        Debug.Log(s[j]) ;
        Debug.Log("----------------------------");
        for (int j = size.y - 1; j >= 0; j--)
        Debug.Log(s1[j]) ;
    }
    public List<List<Vector2Int>> FindSnapPos(BlockInfo info)
    {
        if (info == null) return null;
        int num = info.getNumOfBlock(), count = 0;
        List<List<Vector2Int>> rs = new List<List<Vector2Int>>(100);
        Vector2Int mask = GameManager.instance.blockMaxSize;

        for (int i = 0; i < size.x; i++)
            for (int j = 0; j < size.y; j++)
            {
                if (rs.Count == count) rs.Add(new List<Vector2Int>(num));
                if (rs[count].Count < num) rs[count].Clear();
                for (int k = 0; k < mask.x; k++)
                {
                    for (int l = 0; l < mask.y; l++)
                    {
                        int X = i + k, Y = j + l;
                        if (X < size.x && Y < size.y)
                        {
                            int bd = info.data[k * mask.x + l],
                             gd = gameData[X, Y];
                            if (bd == 1 && gd == 0)
                            {
                                rs[count].Add(new Vector2Int(X, Y));
                                if (rs[count].Count == num)
                                {
                                    count++;
                                }
                            }
                        }

                    }
                }
            }
        if (count == 0) return null;
        else if (rs[rs.Count - 1].Count < num) rs.RemoveAt(rs.Count - 1);
        return rs;
    }
    public void tryData(List<Vector2Int> list, int value)
    {
        rows.Clear();
        cols.Clear();
        foreach (Vector2Int v in list)
        {
            tempData[v.x, v.y] = value;
            AddListColCheck(v.x);
            AddListRowCheck(v.y);
        }
        rowsToClear.Clear();
        colsToClear.Clear();
        rowsToScore.Clear();
        colsToScore.Clear();
        CheckData();
        RocketCheck();
    }
    public void setData(List<Vector2Int> list, int value)
    {

        string s = "dropBlock";
        int r = Random.Range(0,4);
        AudioManager.instance.play(s + r.ToString());
        moveCount++;
        rows.Clear();
        cols.Clear();
        
        foreach (Vector2Int v in list)
        {
            score++;
            gameData[v.x, v.y] = value;
            tempData[v.x, v.y] = value;
            AddListColCheck(v.x);
            AddListRowCheck(v.y);
            equipmentSpaceList.Add(v);
            setBlockPos.Add(v);
        }
        rowsToScore.Clear();
        colsToScore.Clear();
        CheckData();
        RocketCheck();
        UpdateData();
        ScoreOnClear();

        
        //ReadGameData();
        //if (Equipment.OnPlayerCheckClearEvent != null) Equipment.OnPlayerClearEvent.Invoke(rowsToScore, colsToScore);
    }
    public void setData(Vector2Int v, int value)
    {
        gameData[v.x, v.y] = value;
        tempData[v.x, v.y] = value;
    }
    void UpdateData()
    {
        Vector2Int size = GameManager.instance.currentInfo.size ;
        if (rowsToClear.Count == 0 && colsToClear.Count == 0) return;
        foreach (int jRow in rowsToClear)
        {
            clearRow(jRow, 0, false);
            equipmentSpaceList.RemoveAll(v => v.y == jRow);
            rocketList.RemoveAll(v => v.y == jRow);
           // for(int i = 0 ; i<size.y ; i++)
              //  GameManager.instance.currentInfo.gameData[jRow * size.y + i].type = (EquipmentType)0 ;
        }

        foreach (int iCol in colsToClear)
        {
            clearCol(iCol, 0, false);
            equipmentSpaceList.RemoveAll(v => v.x == iCol);
            rocketList.RemoveAll(v => v.x == iCol);

           // for(int i = 0 ; i<size.y ; i++)
               // GameManager.instance.currentInfo.gameData[i * size.y + iCol].type = (EquipmentType)0 ;
        }

        //SaveGameData();
    }
    public void backUpData()
    {
        for (int i = 0; i < size.x; i++)
            for (int j = 0; j < size.y; j++)
                tempData[i, j] = gameData[i, j];
    }
    public void CheckData()
    {
        
        if (rows.Count == 0 && cols.Count == 0) return;
        foreach (int i in rows)
        {
            if (getRow(i) >= size.x && !rowsToScore.Contains(i))
            {
                rowsToScore.Add(i);
            }
        }
        foreach (int j in cols)
        {
            if (getCol(j) >= size.y && !colsToScore.Contains(j))
            {
                colsToScore.Add(j);
            }
        }
        if (rowsToScore.Count == 0 && colsToScore.Count == 0)
        {
            isSpawmEquipment = true;
            setBlockPos.Clear();
        } 
        else
        {   
            isSpawmEquipment = false;
            
        } 
        rows.Clear();
        cols.Clear();
        rowsToClear = rowsToScore.ToList();
        colsToClear = colsToScore.ToList();
    }
    void RocketCheck()
    {
        if(rowsToScore.Count == 0 && colsToScore.Count == 0) return ;
        rocketFireList.Clear();
       
        int count;
        do
        {
            count = rocketFireList.Count;
            foreach (Vector2Int v in rocketList)
            {
                if (!rocketFireList.Contains(v))
                {
                    if (rowsToClear.Contains(v.y))
                    {
                        rocketFireList.Add(v);
                        if (!colsToClear.Contains(v.x))
                        {
                            colsToClear.Add(v.x);
                        } 
                    }
                    if (colsToClear.Contains(v.x))
                    {
                        rocketFireList.Add(v);
                        if (!rowsToClear.Contains(v.y)) 
                        {
                            rowsToClear.Add(v.y);
                        }
                    }
                }
            }
        }
        while (rocketFireList.Count > count);
       
    }

    public void isBoomRemove(Bomb bomb)
    {
        if (mode != PlayModeType.Bomb) return ;
            if (rowsToClear.Contains(bomb.pos.y) || colsToClear.Contains(bomb.pos.x))
            bomb.isCanRemove = true ;
   
    }
    public void ScoreOnClear()
    {
        if (rowsToScore.Count != 0 || colsToScore.Count != 0)
        {
            AudioManager.instance.play("ClearBlock");
            int scoreCount = 0 , posCount = 0;
            scoreCount = rowsToScore.Count + colsToScore.Count ;
            scorePos = Vector2.zero;
            foreach(Vector2Int v in setBlockPos)
            {
                if(rowsToScore.Contains(v.y) || colsToScore.Contains(v.x))
                {
                    GridCellUI c = MyGrid2D.cellUIs[v.x , v.y];
                    scorePos += c.transform.position ;
                    posCount ++ ;
                }
            }

            scorePos = scorePos / posCount ;
            score += scoreValues[scoreCount - 1];
            GameObject scoreObject = ObjectPooler.instance.SpawnFromPool("Score",scorePos) ;
            Score scoreNumber = scoreObject.GetComponent<Score>();
            scoreNumber.Display(scoreColor , scoreValues[scoreCount - 1]);
            scoreNumber.OnSpawn();
            if (scoreCount > 1)
            {
                ScoreType type = (ScoreType)(scoreCount - 2);
                AudioManager.instance.play("sound_" + type.ToString());
                MyGrid2D.OnPlayerScore(type, scorePos + Vector3.up * 0.8f , scoreColor);
            }
        }
    }

    public int GetScore()
    {
        return score;
    }
    #endregion


    #region Game Equipment
    public void SpawnEquipmentCounter()
    {
        if (!isSpawmEquipment) return;
        int timerCount, bombCount, randomRocket;
        if (mode == PlayModeType.Timer)
        {
            timerCount = moveCount % 3;
            if (timerCount == 0) SpawnTimer();
        }
        else if (mode == PlayModeType.Bomb)
        {
            bombCount = moveCount % bombStep;
            if (bombCount == 0 && bombList.Count < PuzzleGame.maxBomb) SpawnBomb();
        }
        if (mode != PlayModeType.Classic8)
        {
            randomRocket = Random.Range(0, 10);
            if (randomRocket < 2 && rocketList.Count < PuzzleGame.maxRocket) SpawnRocket();
        }
    }
    public void ClearEquiment()
    {
        ObjectPooler.instance.deActiveAll();
    }
    void SpawnTimer()
    {
        if (equipmentSpaceList.Count == 0) return;
        Vector2Int v;
        int num = Random.Range(0, 4);
        if (num > equipmentSpaceList.Count) num = equipmentSpaceList.Count;
        for (int i = 0; i < num; i++)
        {
            int random = Random.Range(0, equipmentSpaceList.Count);
            v = equipmentSpaceList[random];
            UIManager.SpawnTimerEvent.Invoke(v);
            equipmentSpaceList.Remove(v);
            //  equipmentList.Add(v);
            gameData[v.x, v.y] = (int)EquipmentType.Timer;
            tempData[v.x, v.y] = gameData[v.x, v.y];
        }
    }

    void SpawnBomb()
    {
        if (equipmentSpaceList.Count == 0) return;
        Vector2Int v;
        int random = Random.Range(0, equipmentSpaceList.Count);
        v = equipmentSpaceList[random];
        UIManager.SpawnBombEvent.Invoke(v);
        equipmentSpaceList.Remove(v);
        bombList.Add(v);
        //  equipmentList.Add(v);
        gameData[v.x, v.y] = (int)EquipmentType.Bomb;
        tempData[v.x, v.y] = gameData[v.x, v.y];
    }

    void SpawnRocket()
    {
        if (equipmentSpaceList.Count == 0) return;
        Vector2Int v;
        int num = Random.Range(1, 2);
        if (num > equipmentSpaceList.Count) num = equipmentSpaceList.Count;
        for (int i = 0; i < num; i++)
        {
            int random = Random.Range(0, equipmentSpaceList.Count);
            v = equipmentSpaceList[random];
            UIManager.SpawnRocketEvent.Invoke(v);
            equipmentSpaceList.Remove(v);
            rocketList.Add(v);
            // equipmentList.Add(v);
            gameData[v.x, v.y] = (int)EquipmentType.Rocket;
            tempData[v.x, v.y] = gameData[v.x, v.y];
        }
    }
    #endregion
}


