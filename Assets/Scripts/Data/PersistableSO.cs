﻿using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class PersistableSO : MonoBehaviour
{
    public static PersistableSO instance;
    [Header("Meta")]
    public string[] persisterNames;
    public static bool isNewPlayer;
    [Header("Scriptable Objects")]
    public List<GamePlayInfo> objectsToPersist;

    void Awake()
    {
        if (instance == null) instance = this;
       
    }
    public void OnStart()
    {
		if (File.Exists(Application.persistentDataPath + string.Format("/Old Player")))
        {
            isNewPlayer = false;
        }
        else
        {
            isNewPlayer = true;
            InitData();
        }
    }
    protected void OnDisable()
    {
        GameManager.instance.SaveCurrentData();
        if(GameManager.instance.currentInfo != null) SaveDataSO(GameManager.instance.currentInfo);
    }

    public void InitData()
    {
        FileStream file;
        BinaryFormatter bf = new BinaryFormatter();
        for(int index = 0 ; index < 4 ; index ++)
        {
            GamePlayInfo info = new GamePlayInfo();
            info.Reset();
            info.playMode = (PlayModeType)index;
            info.playerName = info.playMode.ToString();
            if (info.playMode != PlayModeType.Classic10)
                info.size = new Vector2Int(8, 8);
            else info.size = new Vector2Int(10, 10);
			if(info.isFirstPlay)
            {
                //gamePlayInfos[i].Reset();
                info.isFirstPlay = false ;

                info.slotCheck = new bool[3] ;
                info.spawnBlock = new BlockInfo[3];
                Vector2Int size = info.size ;
                info.gameData = new CellInfo[size.x * size.y];   
                for(int j = 0 ; j < size.x ; j ++)
                    for(int k = 0 ; k < size.y ; k++)
                        info.gameData[j * size.x + k] = new CellInfo();
                for(int j = 0 ; j < 3 ; j++)
                {   
		            BlockInfo blockInfo = BlockInfo.CreateRandomBlockInfo();
                    info.spawnBlock[j] = blockInfo;  
                    info.fileName = info.playMode.ToString();
                    info.slotCheck[j] = true ;
                }
            }
            Debug.Log("first init data");
            file = File.Create(Application.persistentDataPath + string.Format("/{0}.pso", persisterNames[index]));
            var json = JsonUtility.ToJson(info);
            
            bf.Serialize(file, json);
            file.Close();
        }
        file = File.Create(Application.persistentDataPath + string.Format("/Old Player"));
    }
    public void SaveDataSO(GamePlayInfo info)
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file;
        int index = (int)info.playMode;
        if (File.Exists(Application.persistentDataPath + string.Format("/{0}.pso", persisterNames[index])))
        {
            file = File.Open(Application.persistentDataPath + string.Format("/{0}.pso", persisterNames[index]), FileMode.Open);
        }
        else
        {
            file = File.Create(Application.persistentDataPath + string.Format("/{0}.pso", persisterNames[index]));
        }
        var json = JsonUtility.ToJson(info);
        bf.Serialize(file, json);
        file.Close();
    }

    public GamePlayInfo LoadDataSO(PlayModeType mode)
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file;
        GamePlayInfo info = new GamePlayInfo();
        int index = (int)mode;
        if (File.Exists(Application.persistentDataPath + string.Format("/{0}.pso", persisterNames[index])))
        {
            file = File.Open(Application.persistentDataPath + string.Format("/{0}.pso", persisterNames[index]), FileMode.Open);
            JsonUtility.FromJsonOverwrite((string)bf.Deserialize(file), info);
            file.Close();
        }
        else
        {
            Debug.LogWarning("File does not exits");
        }
        return info;
    }
    public void ResetDataSO(int index)
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file;
        if (File.Exists(Application.persistentDataPath + string.Format("/{0}.pso", persisterNames[index])))
        {

            file = File.Open(Application.persistentDataPath + string.Format("/{0}.pso", persisterNames[index]), FileMode.Open);


        }
        else
        {
            file = File.Create(Application.persistentDataPath + string.Format("/{0}.pso", persisterNames[index]));
        }
        objectsToPersist[index].Reset();

        var json = JsonUtility.ToJson(objectsToPersist[index]);
        bf.Serialize(file, json);
        file.Close();
    }
}
