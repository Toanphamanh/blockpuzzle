﻿using UnityEngine;

[System.Serializable]
public class GamePlayInfo  {
	public static GamePlayInfo lastInfo ;
	public string playerName ;
	public int bestscore , score , lastScore;
	public CellInfo[] gameData ;

	
	public PlayModeType playMode ;
	public float timeLeft , maxTime;
	public int moveCount ;
	public bool isFirstPlay ;
	public Vector2Int size ;
	public string fileName ;
	public BlockInfo[] spawnBlock ;
	public bool[] slotCheck ;
	public GameStatus status ;
	public GameEnd gameEnd ;
	public GameNotice gameNotice ;
	public bool isCanRevive ;
	public void Reset()
	{
		playerName = playMode.ToString();
		bestscore = 0 ;
		score = 0 ;
		moveCount = 0 ;
		timeLeft = 121 ;
		maxTime = 120 ;
		lastScore = 0 ;
		gameData = new CellInfo[size.x * size.y];
		for(int i = 0 ; i < size.x ; i ++)
			for(int j = 0 ; j < size.y ;j ++)
			{
				gameData[i * size.x + j] = new CellInfo();
				gameData[i * size.x + j].type = EquipmentType.None ;
				gameData[i * size.x + j].counter = 0;
			}
		slotCheck = new bool[3];
		for(int j = 0 ; j < 3 ; j++)
                {   
		           // BlockInfo info = BlockInfo.CreateRandomBlockInfo();
                   // spawnBlock[j] = info;  
                   // fileName = playMode.ToString();
                    slotCheck[j] = true ;
                }
		isCanRevive = true ;
		isFirstPlay = true;
		gameEnd = GameEnd.None ;
		status = GameStatus.Reset ;
	}
	public void ResetLevel()
	{
		timeLeft = 121 ;
		moveCount = 0 ;
		status = GameStatus.Reset ;
		lastScore = score ;
		if(score > bestscore ) bestscore = score ;
		score = 0;
		isCanRevive = true ;
		gameEnd = GameEnd.None ;
		
		for(int i = 0 ; i < size.x ; i ++)
			for(int j = 0 ; j < size.y ;j ++)
			{
				gameData[i * size.x + j].type = EquipmentType.None ;
			}
	}
}
[System.Serializable]
public class CellInfo
{
	public EquipmentType type ;
	public BlockColor color ;
	public int counter;
}
[System.Serializable]
public enum EquipmentType
{
	None = 0 ,
	Normal = 1 ,
	Timer = 2 ,
	Rocket = 3 ,
	Bomb = 4 
}
[System.Serializable]
public enum PlayModeType
{
	Classic10 = 0  ,
	Classic8 = 1 ,
	Timer = 2 ,
	Bomb = 3
}
