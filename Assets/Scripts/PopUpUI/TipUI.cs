﻿using UnityEngine;
using UnityEngine.UI;
using DG.Tweening ;
using UnityEngine.EventSystems ;
public class TipUI : MonoBehaviour , IPointerClickHandler 
{
    [SerializeField] Sprite[] tipSprites ;
    [SerializeField] Image tipImg , tipPanel;
    [SerializeField] Image tipBtn ;
    public void Display()
    {
        PlayModeType mode ;
        mode = GameManager.instance.currentInfo.playMode ;
        int i = (int) mode ;
        PopUpManager.instance.turnOffPauseBtn();
        
        if(i < 2) 
        
        {
            
            PopUpManager.instance.turnOnPauseBtn ();
            PopUpManager.instance.PopUpStartGame().Restart();
            
            return ;
        } 
        i = mode == PlayModeType.Timer ? 0 : 1 ;
        tipImg.sprite = tipSprites[i] ;
        tipImg.SetNativeSize();
        transform.position = Vector3.up ;
        transform.localScale = Vector3.zero ;
        
        transform.FadeIn(0.5f);
        GameManager.instance.isGamePlay = false ;
        PopUpManager.instance.turnOnPanel();

    }
    public void OnPointerClick(PointerEventData eventData)
    {
       // GameManager.instance.GamePlayStatusChangeEvent.Invoke(GameStatus.Resume);
       Sequence s = DOTween.Sequence().SetAutoKill(false).Pause();
       tipPanel.raycastTarget = false ;
       s.Append(transform.FadeOut(0.5f));
       if(GameManager.instance.isGameStart) 
            GameManager.instance.isGamePlay = true ;
       else
        s.Append(PopUpManager.instance.PopUpStartGame());
       s.AppendCallback(
           () => 
            {

                PopUpManager.instance.turnOnPauseBtn ();
                PopUpManager.instance.turnOffPanel();
                PopUpManager.instance.turnOnTipBtn();
                tipPanel.raycastTarget = true ;
            }
       ).Restart();
    }
}
