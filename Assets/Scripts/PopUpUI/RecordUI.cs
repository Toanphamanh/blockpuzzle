﻿using UnityEngine;
public class RecordUI : MonoBehaviour
{
    public Animator anim ;
    TextNumberHelper textHelper ;
    int value ;
    void Start()
    {
        textHelper = GetComponentInChildren<TextNumberHelper>();
        textHelper.value = 0 ;
    }
    public void OnAppear()
    {
        textHelper.value = 0 ;
        textHelper.Init();
        textHelper.Display();
        value = GameManager.instance.currentInfo.bestscore ;
        textHelper.RollNumber(value , 2f) ;
        UIManager.instance.UpdateBestScoreTxt();
    }
}
