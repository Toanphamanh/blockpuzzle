﻿using UnityEngine;
using DG.Tweening;
using System.Collections;

public class Score : MonoBehaviour , IPooledObject
{
    TextNumberHelper tnh ;
    // Start is called before the first frame update
    float acc = 12f ;
    void Awake()
    {
        tnh = GetComponent<TextNumberHelper>();
    }

    public void Display(BlockColor color , int score)
    {
        tnh.value = score ;
        tnh.ChangeSprites((int) color) ;
        tnh.Init();
        tnh.Display();
    }
    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnSpawn()
    {
       StartCoroutine(PopUp(4f))
       ;
    }

    IEnumerator PopUp(float duration)
    {
        transform.localScale = Vector3.zero ;
        float time = 0 , scale = 0f , speed = 0f; 
        bool isIn = true  , isOut = false , isBack = false;
        while(time < duration)
        {
            time += Time.deltaTime ;
            if(scale < 1.2f && isIn)
                speed = acc*time ;
            else if(scale >= 1.2f && isIn)
            {
                isIn = false ;
                isOut = true ;
                yield return new WaitForSeconds(0.1f);
                acc = 8f ;
            }
           
            if(isOut)
            {
                if(scale > 0.5f) speed -= acc*Time.deltaTime ;
                else
                speed = 0f ;
            }
                
            scale = speed * time / 2 ;
            
            transform.localScale = Vector3.one * scale ;
            if(scale == 0 ) gameObject.SetActive(false);
            if(scale <= 1 && isOut && !isBack)
            {
                isBack = true ;
                yield return new WaitForSeconds(0.5f);
            } 

            yield return new WaitForFixedUpdate() ;
            
        }
        yield return null ;
    }

    public void OnDespawn()
    {
        throw new System.NotImplementedException();
    }
}
