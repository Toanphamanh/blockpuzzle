﻿using UnityEngine;
using UnityEngine.UI;
using DG.Tweening ;
//
public class ScoreUI : MonoBehaviour , IPooledObject
{
    Image scoreImg ;
    public ScoreType type ;
    float lifeTime = 4f;
    float upSpeed = 1f , time;
    int score ;
    int[] index ;
    public BlockColor color ;
    bool isStartMove   , isMoveBack  , isMoveEnd  ;

    // Start is called before the first frame update

    // Update is called once per frame
    void Update()
    {
        if(!gameObject.activeSelf) return;
        if(lifeTime > 0)
        {
            lifeTime -= Time.deltaTime ;
        }
        else
        {
            transform.FadeOut(0.6f).Restart();
        }
    }

    public void Start()
    {
        
    }

    void move()
    {
        float baseX = transform.position.x , dir;
        if(Mathf.Abs(baseX) < 1) baseX = 1f ;
        dir = baseX > 0 ? 1 : -1;
        Sequence s = DOTween.Sequence().SetAutoKill(false).Pause();
        s.Append(transform.DOMoveX(-baseX , 1f).SetRelative().SetEase(Ease.InOutQuad))
            .Append(transform.DOMoveX(baseX*0.2f , 0.2f)).SetRelative().SetEase(Ease.InOutQuad)
                .Append(transform.DOMoveX(-dir * 5f ,2f)).SetRelative().SetEase(Ease.InOutQuad).Restart();
    }

    public void OnDespawn()
    {
        throw new System.NotImplementedException();
    }

    public void OnSpawn()
    {
        lifeTime = 4f;
        isStartMove = true ;
        isMoveBack = false ;
        isMoveEnd = false ;

        scoreImg = GetComponent<Image>();
        scoreImg.sprite = UIManager.instance.scoreSprites[(int)type];
        score = PuzzleGame.scoreValues[(int)type] ;
        //transform.FadeIn(0.6f).Append(transform.FlyOut(1f , Vector3.up)).Restart();
        move();
    }
}

public enum ScoreType
{
    Good = 0,
    Great = 1,
    Amazing = 2,
    Excellent = 3
}
