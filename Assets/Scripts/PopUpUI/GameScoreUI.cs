﻿using UnityEngine;
using UnityEngine.UI;
public class GameScoreUI : MonoBehaviour
{

    [SerializeField] Image modeIcon ;
    [SerializeField] Sprite[] modeIcons ;
    [SerializeField] TextNumberHelper best , score ;

    public void ChangeIcon()
    {
        modeIcon.sprite = modeIcons[(int)GameManager.instance.currentInfo.playMode] ;
        modeIcon.SetNativeSize();
    }

    public void reset()
    {
        best.value = 0 ;
        score.value = 0 ;
        best.Init() ;
        best.Display();
        score.Init();
        score.Display();
    }
    public void OnPopUp()
    {
        best.RollNumber(GameManager.instance.currentInfo.bestscore , 0.75f);
        score.RollNumber(GameManager.instance.currentInfo.score , .75f) ;
        AudioManager.instance.play("Score" , .75f);
    }
}
