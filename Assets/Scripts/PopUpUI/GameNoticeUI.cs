﻿using UnityEngine;
using UnityEngine.UI;

public class GameNoticeUI : MonoBehaviour
{
    [SerializeField] Sprite[] noticeSprites ;
    Image noticeImg , bgImg ;
    public static GameNotice notice ;
    // Start is called before the first frame update
    void Start()
    {
        bgImg = GetComponent<Image>();
        noticeImg = transform.GetChild(0).transform.GetComponent<Image>();
        bgImg.raycastTarget = false ;
        noticeImg.color = new Color(0,0,0,0);
    }

    // Update is called once per frame
    public void Display( GameNotice n)
    {
        noticeImg.color = Color.white ;
        bgImg.raycastTarget = true ;
        noticeImg.sprite = noticeSprites[(int)n];
    }
}

public enum GameNotice
{
    TimeisUp ,
    NoMove ,
    BombBlast ,
    Pause 
}
