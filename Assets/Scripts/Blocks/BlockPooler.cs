﻿using UnityEngine;
using System;
using System.Collections.Generic;

public class BlockPooler : MonoBehaviour
{
    public static BlockPooler Instance ;
    public int blockTypeCount = BlockInfo.typeNum ,
               blockColorNum = BlockInfo.colorNum ;
    [System.Serializable]
    public class BlockPool
    {
        public string tag ;
        public BlockColor blockColor ;
        public BlockType blockType ;

        public int position ;
        public int count ;

    }
    string[] blockTypeNames = Enum.GetNames(typeof(BlockType)) ;
    string[] blockColorNames = Enum.GetNames(typeof(BlockColor)) ;
    [SerializeField]    public List<BlockPool> pools ;
    public Dictionary<string , Queue<GameObject>> poolDictionary ;

    void Awake()
    {
        if(Instance == null) Instance = this ;
    }
    void Start()
    {
        poolDictionary = new Dictionary<string, Queue<GameObject>>(blockTypeCount) ;
        pools = new List<BlockPool>(blockTypeCount * blockColorNum) ;
        for(int i = 0 ; i < blockTypeCount ; i++)
        {
            int positionCount = BlockInfo.getData((BlockType)i).Length ;
            for(int j = 0 ; j < blockColorNum ; j ++)
            {
                for(int k = 0 ; k < positionCount ; k++)
                {
                    BlockPool pool = new BlockPool() ;
                    pool.tag = blockTypeNames[i]+ "-" + blockColorNames[j] + "-"+ k.ToString() ;
                    pool.blockType = (BlockType)i ;
                    pool.blockColor = (BlockColor)j ;
                    pool.position = k  ;
                    pools.Add(pool) ;
                }
                
            }
        }

        foreach(BlockPool pool in pools)
        {
            pool.count = 4 ;
            Queue<GameObject> queu = new Queue<GameObject>(pool.count) ;
            for(int i = 0 ; i < pool.count ; i++)
            {
                GameObject block = new GameObject (pool.blockType.ToString() +"-"+pool.blockColor.ToString()+ "-" + i.ToString()) ;
                BlockDisplaySprite bdUI = block.AddComponent<BlockDisplaySprite>();
                BlockInfo info = new BlockInfo(pool.blockColor , pool.blockType , pool.position) ;
                bdUI.info = info ;
                block.AddComponent<BlockClick>() ;
                block.AddComponent<BlockDrag>();
                bdUI.Init();

                
                block.SetActive(false) ;
                queu.Enqueue(block) ;
            }
            poolDictionary.Add(pool.tag , queu) ;

        }
    }
    public GameObject RepawnBlock(BlockInfo info , Grid slot, int index ) 
    {
        string tag = info.type.ToString() + "-" + info.color.ToString() + "-" + info.position.ToString() ;
        if(!poolDictionary.ContainsKey(tag))
        {
            Debug.Log("Null Block");
            return null;
        }
        GameObject block = poolDictionary[tag].Dequeue() ;
        
        BlockDisplaySprite bdSprite = block.GetComponent<BlockDisplaySprite>();
        bdSprite.usingGrid = slot ;
        bdSprite.slotIndex = index ;
        bdSprite.info = info ;
        block.transform.position = slot.transform.position ;
        poolDictionary[tag].Enqueue(block) ;
        return block ;
    }
}
