﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class BlockDisplaySprite : MonoBehaviour , IPooledObject {
	public Vector2 blockSize ;
	int  n;
	public Vector2Int size ; // real size of blocks
	public List<Vector3Int> blocksCellPos ; // position of cell in grid
	public List<GameObject> blockObjs ; // 
	public Grid usingGrid ;
	public BlockInfo info ;
	public int slotIndex , blockID;
	public bool isSnap , isCanSnap ;
	static Color unMoveColor ;
	public static UnityEvent GameDataChangeEvent ;
	public static int blockIDCount ;
	public BoxCollider2D collider ;
	public Grid grid ;

	BlockDrag bd ;
	BlockClick bc ;
	public void Init()
	{
		unMoveColor = new Color(0.5f,0.5f,0.5f,1f) ;
		blockIDCount ++ ;
		blockID = blockIDCount ;
		usingGrid = UIManager.instance.bottomGrids[0] ;
		InitBlock();
		SetUpBlocks();
		
		if(GameDataChangeEvent == null)
		{
			GameDataChangeEvent = new UnityEvent();
		}	
		GameDataChangeEvent.AddListener(this.OnGameDataChange);	
		//Debug.Break();
	}
    void InitBlock()
	{
		isSnap = false ;
		if (info.data == null) return ;
		CheckSnap();
		n = 0 ;
		blocksCellPos = new List<Vector3Int>(25);
		int x = 0,y = 0 ;
		for(int i = 0 ; i < 5 ; i++)
			for(int j = 0 ; j < 5 ; j++)
			{
				if(info.data[i*5 + j] == 1) 
				{
					x = i + 1;
				    if(y < j + 1) y = j + 1;
					blocksCellPos.Add(new Vector3Int(-5/2 + i , -5/2 + j , 0));
					n ++ ;
				}
			}
		size = new Vector2Int(x,y); ///  ??
		
		collider = gameObject.AddComponent<BoxCollider2D>(); 
        blockSize = new Vector2(size.x*usingGrid.cellSize.x * .01f , size.y*usingGrid.cellSize.y * .01f);
		collider.offset = Vector2.zero;
		collider.size = new Vector2(5*usingGrid.cellSize.x * .01f, 5*usingGrid.cellSize.y * .01f);
		//collider.autoTiling = true ;
		
	}
	public void CheckSnap()
	{
		if(GameManager.instance.game == null) return ;
		List<List<Vector2Int>> list = GameManager.instance.game.FindSnapPos(this.info) ;
		if(list == null)
		{
			this.isCanSnap = false ;
			return;
		} 
		else isCanSnap = true ;
	}
	void SetUpBlocks()
	{
		float scaleX = usingGrid.cellSize.x / UIManager.instance.blockSprites[0].rect.width ,
		      scaleY = usingGrid.cellSize.y / UIManager.instance.blockSprites[0].rect.height   ; 
	    grid = gameObject.GetComponent<Grid>() ;
		if(grid == null)
		grid = gameObject.AddComponent<Grid>();
		grid.cellSize = blockSize / size ;
		for(int i = 0 ; i < n ; i++) 
		{
			GameObject ob = new GameObject("block");
            ob.transform.SetParent(transform);
			SpriteRenderer sr = ob.AddComponent<SpriteRenderer>();
			sr.sprite = UIManager.instance.blockSprites[(int)info.color] ;
            sr.color = Color.white ;
			sr.color = isCanSnap ? Color.white : unMoveColor;
           
			blocksCellPos[i] = new Vector3Int(blocksCellPos[i].x + (5-size.x)/2 , blocksCellPos[i].y + (5-size.y)/2 , 0);
			Vector3 posCenter = grid.GetCellCenterWorld(blocksCellPos[i]) ,
					pos = grid.CellToWorld(blocksCellPos[i]) ;
			float x = size.x % 2 == 0 ? posCenter.x : pos.x ,
				  y = size.y % 2 == 0 ? posCenter.y : pos.y ;
			ob.transform.position = new Vector3(x , y , 0);
			//ob.transform.localPosition = new Vector3(ob.transform.localPosition.x , ob.transform.localPosition.y , 0) ;
			ob.gameObject.name = blockID.ToString() ;
			ob.transform.localScale = new Vector3(scaleX , scaleY , 1 );

		}
		bd = gameObject.GetComponent<BlockDrag>();
		bc = gameObject.GetComponent<BlockClick>();
		blockObjs = GetBlockCell();
		//Debug.Break();
		//int max = size.x > size.y ? size.x : size.y ;
		//;
	}

	void Display()
	{
		int i = 0 ;
		foreach ( GameObject ob in blockObjs)
		{
			
			Vector3 posCenter = grid.GetCellCenterWorld(blocksCellPos[i]) ,
					pos = grid.CellToWorld(blocksCellPos[i]) ;
			float x = size.x % 2 == 0 ? posCenter.x : pos.x ,
				  y = size.y % 2 == 0 ? posCenter.y : pos.y ;
			ob.transform.position = new Vector3(x , y , 0);

			i++ ;
		}
			
	}
	public List<GameObject> GetBlockCell()
	{
		int n = transform.childCount;
		List<GameObject> rs = new List<GameObject>(n);
		for(int i = 0 ; i < n ; i++)
		{
			GameObject gOb = transform.GetChild(i).gameObject;
			rs.Add(gOb);
		}
		return rs;
	}
	public List<Vector3> GetBlocksPos()
    {
        Vector3 pos  ;
        List<Vector3> rs = new List<Vector3>(blockObjs.Count);
        foreach ( GameObject gOb in blockObjs )
        {
            pos = gOb.transform.position ;
			rs.Add(pos);
        }
		return rs ;
    }

	public void setColor(Color c)
	{
		if(blockObjs.Count == 0 ) return ;
		foreach(GameObject bo in blockObjs)
		{
			SpriteRenderer sr = bo.GetComponent<SpriteRenderer>();
				sr.color = c;
		}
	}
	private void OnGameDataChange()
    {
		if(!gameObject.activeSelf) return;
        CheckSnap() ;
		Color c = isCanSnap ? Color.white : unMoveColor ;
		setColor(c);
    }

	public void ChangeBlockSize( Vector3 cellSize , Vector3 cellGap)
	{
		grid.cellSize = cellSize ;
		grid.cellGap = cellGap ;
		Display();
	}
	public void OnSpawn()
	{
		gameObject.SetActive(true) ;
		isSnap = false ;
		ChangeBlockSize(blockSize/size , Vector3.zero);
		blockMoveRespawn()	;
		
		OnGameDataChange() ;

	}

	public void blockMoveRespawn()
	{
		bd.OnRespawn();
		bc.OnRespawn();
	}
	public void OnDespawn()
	{
		bc.OnDeSpawn();
		gameObject.SetActive(false) ;
	}

    private class EditorApplication
    {
    }
}
