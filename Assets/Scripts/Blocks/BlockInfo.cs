﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

[System.Serializable]
public class BlockInfo  {
	public	BlockColor color ;
	public BlockType type ;	
	public int positionCount ;
	public int position;
	public int[] data ;

	
	[SerializeField] GameSetting setting ;
	static bool isInit = false ;
	public static int colorNum = Enum.GetNames(typeof (BlockColor)).Length ,
		              typeNum = Enum.GetNames(typeof(BlockType)).Length ;
	public static int[][] DOT = new int[1][], MINUS = new int[2][] , IBLOCK = new int[2][] , JBLOCK = new int [4][] , LBLOCK = new int[4][] ,
				 SBLOCK = new int[2][] , ZBLOCK = new int[2][], OBLOCK = new int[1][], TBLOCK = new int[4][] ,SQUARE = new int[1][] ,
				 RECTANGLE = new int[2][] , V3x3 = new int[4][] , I5 = new int[2][] , V2x2 = new int[4][] , I3 = new int [2][];
	public static void Init()
	{
		DOT[0] = new int[25] { 1 , 0 , 0 , 0 , 0 ,
	  			          	   0 , 0 , 0 , 0 , 0 ,
	 			          	   0 , 0 , 0 , 0 , 0 ,
	 			          	   0 , 0 , 0 , 0 , 0 ,
				         	   0 , 0 , 0 , 0 , 0 } ;
		MINUS[0] = new int[25] 	   { 1 , 1 , 0 , 0 , 0 ,
	 			                     0 , 0 , 0 , 0 , 0 ,
				                     0 , 0 , 0 , 0 , 0 ,
				                     0 , 0 , 0 , 0 , 0 , 
				                     0 , 0 , 0 , 0 , 0 } ;
		MINUS[1] = new int[25] 	   { 1 , 0 , 0 , 0 , 0 ,
	 			                     1 , 0 , 0 , 0 , 0 ,
	 			                     0 , 0 , 0 , 0 , 0 ,	
	 			                     0 , 0 , 0 , 0 , 0 ,
									 0 , 0 , 0 , 0 , 0	} ;
		IBLOCK[0] = new int[25]		{ 1 , 1 , 1 , 1 , 0 , 
	 	          					  0 , 0 , 0 , 0 , 0 ,
	 								  0 , 0 , 0 , 0 , 0 ,
	 								  0 , 0 , 0 , 0 , 0 ,
	 							   	  0 , 0 , 0 , 0 , 0 } ;
		IBLOCK[1] = new int[25] 	{ 1 , 0 , 0 , 0 , 0 ,
	 			  					  1 , 0 , 0 , 0 , 0 ,
	 			  					  1 , 0 , 0 , 0 , 0 ,
	 			   					  1 , 0 , 0 , 0 , 0 ,
	 			   					  0 , 0 , 0 , 0 , 0 } ;
		JBLOCK[0] = new int[25]  	{ 0 , 1 , 0 , 0 , 0 ,
	 			  					  0 , 1 , 0 , 0 , 0 ,
	 			  					  1 , 1 , 0 , 0 , 0 ,
	 			  					  0 , 0 , 0 , 0 , 0 ,
	 			  					  0 , 0 , 0 , 0 , 0 } ;
		JBLOCK[1] = new int[25] 	{ 1 , 0 , 0 , 0 , 0 ,
	 			  					  1 , 1 , 1 , 0 , 0 ,
	 			  					  0 , 0 , 0 , 0 , 0 ,
	 			  					  0 , 0 , 0 , 0 , 0 ,
	 			  					  0 , 0 , 0 , 0 , 0 } ;
		JBLOCK[2] = new int[25]		{ 1 , 1 , 0 , 0 , 0 ,
	 			  					  1 , 0 , 0 , 0 , 0 ,
	 			  					  1 , 0 , 0 , 0 , 0 ,
	 			  					  0 , 0 , 0 , 0 , 0 , 
	 			  					  0 , 0 , 0 , 0 , 0 } ;
		JBLOCK[3] = new int[25]   	{ 1 , 1 , 1 , 0 , 0 ,
				  					  0 , 0 , 1 , 0 , 0 ,
	 			  					  0 , 0 , 0 , 0 , 0 ,
	 			  					  0 , 0 , 0 , 0 , 0 ,
	 			  					  0 , 0 , 0 , 0 , 0 } ;
		LBLOCK[0] = new int[25]	 	{ 1 , 0 , 0 , 0 , 0 ,
	 			  					  1 , 0 , 0 , 0 , 0 ,
	 			   					  1 , 1 , 0 , 0 , 0 ,
	 			   					  0 , 0 , 0 , 0 , 0 ,
	 			   					  0 , 0 , 0 , 0 , 0 }	;
		LBLOCK[1] = new int[25] 	{ 1 , 1 , 1 , 0 , 0 ,
	 	           					  1 , 0 , 0 , 0 , 0 ,
	 			 					  0 , 0 , 0 , 0 , 0 ,
	 			 					  0 , 0 , 0 , 0 , 0 ,
	 			 					  0 , 0 , 0 , 0 , 0 };		
		LBLOCK[2] = new int[25] 	{ 1 , 1 , 0 , 0 , 0 ,
	 			 					  0 , 1 , 0 , 0 , 0 ,
	 			 					  0 , 1 , 0 , 0 , 0 ,
	 			 					  0 , 0 , 0 , 0 , 0 ,
	 			 					  0 , 0 , 0 , 0 , 0 };
		LBLOCK[3] = new int[25] 	{ 0 , 0 , 1 , 0 , 0 , 
	 			 					  1 , 1 , 1 , 0 , 0 ,
	 			 					  0 , 0 , 0 , 0 , 0 ,
	 			 					  0 , 0 , 0 , 0 , 0 ,
	 			 					  0 , 0 , 0 , 0 , 0 } ;
		SBLOCK[0] = new int[25] 	{ 0 , 1 , 1 , 0 , 0 ,
	 			 					  1 , 1 , 0 , 0 , 0 ,
	 			 					  0 , 0 , 0 , 0 , 0 ,
	 			 					  0 , 0 , 0 , 0 , 0 ,
	 			 					  0 , 0 , 0 , 0 , 0 };

		SBLOCK[1] = new int[25] 	{ 1 , 0 , 0 , 0 , 0 ,
	 			 					  1 , 1 , 0 , 0 , 0 ,
	 			 					  0 , 1 , 0 , 0 , 0 ,
	 			 					  0 , 0 , 0 , 0 , 0 ,
	 			 					  0 , 0 , 0 , 0 , 0 } ;
		ZBLOCK[0] = new int[25] 	{ 1 , 1 , 0 , 0 , 0 ,
	 			 					  0 , 1 , 1 , 0 , 0 ,
	 			 					  0 , 0 , 0 , 0 , 0 ,
	 			 					  0 , 0 , 0 , 0 , 0 ,
	 			 					  0 , 0 , 0 , 0 , 0 };
		ZBLOCK[1] = new int[25] 	{ 0 , 1 , 0 , 0 , 0 ,
	 			 					  1 , 1 , 0 , 0 , 0 ,
	 			 					  1 , 0 , 0 , 0 , 0 , 
	 			 					  0 , 0 , 0 , 0 , 0 ,
	 			 					  0 , 0 , 0 , 0 , 0 };
		OBLOCK[0] = new int[25] 	{ 1 , 1 , 0 , 0 , 0 , 
	 			 					  1 , 1 , 0 , 0 , 0 ,
	 			 					  0 , 0 , 0 , 0 , 0 ,
	 			 					  0 , 0 , 0 , 0 , 0 ,
	 			 					  0 , 0 , 0 , 0 , 0 };
		TBLOCK[0] = new int[25] 	{ 1 , 1 , 1 , 0 , 0 ,
	 			 					  0 , 1 , 0 , 0 , 0 ,
	 			 					  0 , 0 , 0 , 0 , 0 ,
	 			 					  0 , 0 , 0 , 0 , 0 ,
	 			 					  0 , 0 , 0 , 0 , 0 };
		TBLOCK[1] = new int[25]  	{ 0 , 1 , 0 , 0 , 0 ,
	 			 					  1 , 1 , 0 , 0 , 0 ,
	 			 					  0 , 1 , 0 , 0 , 0 ,
	 			 					  0 , 0 , 0 , 0 , 0 ,
	 			 					  0 , 0 , 0 , 0 , 0 };
		TBLOCK[2] = new int[25] 	{ 0 , 1 , 0 , 0 , 0 ,
	              					  1 , 1 , 1 , 0 , 0 ,
	 			 					  0 , 0 , 0 , 0 , 0 ,
	 			 					  0 , 0 , 0 , 0 , 0 ,
	 			 					  0 , 0 , 0 , 0 , 0 };
		TBLOCK[3] = new int[25]		{ 0 , 1 , 0 , 0 , 0 ,
	              					  0 , 1 , 1 , 0 , 0 ,
	 			 					  0 , 1 , 0 , 0 , 0 ,
	 			 					  0 , 0 , 0 , 0 , 0 ,
	 			 					  0 , 0 , 0 , 0 , 0 };
		SQUARE[0] = new int[25]     { 1 , 1 , 1 , 0 , 0 ,
									  1 , 1 , 1 , 0 , 0 ,
									  1 , 1 , 1 , 0 , 0 ,
									  0 , 0 , 0 , 0 , 0 ,
									  0 , 0 , 0 , 0 , 0 };
		RECTANGLE[0] = new int[25]  { 1 , 1 , 1 , 0 , 0 ,
									  1 , 1 , 1 , 0 , 0 ,
									  0 , 0 , 0 , 0 , 0 ,
									  0 , 0 , 0 , 0 , 0 ,
									  0 , 0 , 0 , 0 , 0 };
		RECTANGLE[1] = new int[25]  { 1 , 1 , 0 , 0 , 0 ,
									  1 , 1 , 0 , 0 , 0 , 
									  1 , 1 , 0 , 0 , 0 ,
									  0 , 0 , 0 , 0 , 0 ,
									  0 , 0 , 0 , 0 , 0 } ;
		V3x3[0]      = new int[25]  { 1 , 1 , 1 , 0 , 0 ,
									  1 , 0 , 0 , 0 , 0 ,
									  1 , 0 , 0 , 0 , 0 ,
									  0 , 0 , 0 , 0 , 0 ,
									  0 , 0 , 0 , 0 , 0 };
		V3x3[1]      = new int[25]  { 1 , 1 , 1 , 0 , 0 ,
									  0 , 0 , 1 , 0 , 0 ,
									  0 , 0 , 1 , 0 , 0 ,
									  0 , 0 , 0 , 0 , 0 ,
									  0 , 0 , 0 , 0 , 0 } ;
		V3x3[2]      = new int[25]  { 0 , 0 , 1 , 0 , 0 ,
									  0 , 0 , 1 , 0 , 0 ,
									  1 , 1 , 1 , 0 , 0 ,
									  0 , 0 , 0 , 0 , 0 ,
									  0 , 0 , 0 , 0 , 0 };
		V3x3[3]      = new int[25]  { 1 , 0 , 0 , 0 , 0 ,
									  1 , 0 , 0 , 0 , 0 ,
									  1 , 1 , 1 , 0 , 0 ,
									  0 , 0 , 0 , 0 , 0 ,
									  0 , 0 , 0 , 0 , 0 };
		I5[0] = new int [25]        { 1 , 1 , 1 , 1 , 1 ,
									  0 , 0 , 0 , 0 , 0 ,
									  0 , 0 , 0 , 0 , 0 ,
									  0 , 0 , 0 , 0 , 0 ,
									  0 , 0 , 0 , 0 , 0 } ;
		I5[1] = new int[25]			{ 1 , 0 , 0 , 0 , 0 ,
									  1 , 0 , 0 , 0 , 0 ,
									  1 , 0 , 0 , 0 , 0 ,
									  1 , 0 , 0 , 0 , 0 ,
									  1 , 0 , 0 , 0 , 0 };
		V2x2[0] = new int[25]		{ 1 , 0 , 0 , 0 , 0 ,
									  1,  1 , 0 , 0 , 0 ,
									  0 , 0 , 0 , 0 , 0 ,
									  0 , 0 , 0 , 0 , 0 ,
									  0 , 0 , 0 , 0 , 0 };
		V2x2[1] = new int[25]		{ 1 , 1 , 0 , 0 , 0 ,
									  1 , 0 , 0 , 0 , 0 ,
									  0 , 0 , 0 , 0 , 0 ,
									  0,  0 , 0 , 0 , 0 ,
									  0 , 0 , 0 , 0 , 0 };
		V2x2[2] = new int[25]		{ 1 , 1 , 0 , 0 , 0 ,
									  0 , 1 , 0 , 0 , 0 ,
									  0 , 0 , 0 , 0 , 0 ,
									  0 , 0 , 0 , 0 , 0 ,
									  0 , 0 , 0 , 0 , 0 };	
		V2x2[3] = new int[25]		{ 0 , 1 , 0 , 0 , 0 ,
									  1 , 1 , 0 , 0 , 0 ,
									  0 , 0 , 0 , 0 , 0 ,
									  0,  0 , 0 , 0 , 0 ,
									  0 , 0 , 0 , 0 , 0 };	
		I3[0] = new int[25]         { 1 , 1 , 1 , 0 , 0 ,
									  0 , 0 , 0 , 0 , 0 ,
									  0 , 0 , 0 , 0 , 0 ,
									  0 , 0 , 0 , 0 , 0 ,
									  0 , 0 , 0 , 0 , 0 } ;
		I3[1] = new int[25] 		{ 1 , 0 , 0 , 0 , 0 ,
									  1 , 0 , 0 , 0 , 0 ,
									  1 , 0 , 0 , 0 , 0 ,
									  0 , 0 , 0 , 0 , 0 , 
									  0 , 0 , 0 , 0 , 0 };	
			  						  
	}
	public BlockInfo(BlockColor color , BlockType type , int position)
	{
		if(!isInit) Init();
		this.color = color ;
		this.type = type;
		int[][] baseData;

		baseData = getData(type);
		this.positionCount = baseData.Length ;
		if(position == -1) 
		{
			this.position = Random.Range(0,positionCount);
		}
		else this.position = position;
		if(baseData != null)
		this.data = baseData[this.position];
		
	}

	public static int[][] getData(BlockType type)
	{
		switch(type)
		{
			case BlockType.Dot : 
				 return DOT ;
			case BlockType.Minus : 
				 return MINUS ;
			case BlockType.I_block : 
				 return IBLOCK;
			case BlockType.J_block : 
				 return JBLOCK ;
			case BlockType.L_block : 
				 return LBLOCK ;
			case BlockType.S_block : 
				 return SBLOCK;
			case BlockType.Z_block :
				 return ZBLOCK;
			case BlockType.O_block :
				 return OBLOCK;
			case BlockType.T_block :
				 return TBLOCK;
			case BlockType.V3x3 :
				 return V3x3;
			case BlockType.I5 :
				 return I5;
			case BlockType.RECTANGLE :
				 return RECTANGLE ;
			case BlockType.SQUARE :
				 return SQUARE;
			case BlockType.V2x2 :
				 return V2x2 ;
			case BlockType.I3 :
				 return I3;
			default : 
				return null ;
		}
	}

	public static BlockInfo CreateRandomBlockInfo()
	{
		BlockType randomBlock  ;
		int randomColor = UnityEngine.Random.Range(0, BlockInfo.colorNum);
		float random = Random.Range(0f , 1f);
		if( 0 <= random && random < 0.6)
		{
			int r = Random.Range(0 , GameSetting.instance.commonBlockList.Count) ;
			randomBlock = GameSetting.instance.commonBlockList[r] ;
		}
		else if ( 0.6 <= random && random < 0.9)
		{
			int r = Random.Range(0 , GameSetting.instance.rareBlockList.Count) ;
			randomBlock = GameSetting.instance.rareBlockList[r] ;
		}
		else
		{
			int r = Random.Range(0 , GameSetting.instance.superRareBlockList.Count) ;
			randomBlock = GameSetting.instance.superRareBlockList[r];
		} 
		
		return new  BlockInfo((BlockColor)randomColor , (BlockType)randomBlock , -1) ;
 	}
	public int getNumOfBlock()
	{
		Vector2Int size = GameManager.instance.blockMaxSize ;
		
		int s = 0;
		for(int i = 0 ; i < GameManager.instance.blockMaxSize.x ; i++)
			for(int j = 0 ; j < GameManager.instance.blockMaxSize.y ; j++)
			{
				s += this.data[i*size.x + j] ;
			}
		return s ;
	}

}
[System.Serializable]
public enum BlockColor { 
	Blue = 0,Green = 1, Pink = 2, Cygan = 3, Purple = 4, Orange = 5
}

public enum BlockType { 
	Dot = 0, Minus = 1 , I_block = 2 , J_block = 3 , L_block = 4, S_block = 5, Z_block = 6, O_block = 7, T_block = 8, V3x3 = 9 , I5 = 10 ,
	RECTANGLE = 11 , SQUARE = 12 , V2x2 = 13 , I3
}

public enum spawnRateType
{
	common  = 60  ,
	rare = 20 ,
	superRare = 10 
}