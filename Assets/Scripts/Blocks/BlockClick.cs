﻿using UnityEngine;
using DG.Tweening;
public class BlockClick : MonoBehaviour
{
    public bool isDrag, isMouseUp, isOnDrag , isMouseDown;
    [SerializeField]Vector2 delta, beginPos, begin_mPos, m_Pos, last_mPos, moveTarget, moveStart, lastPosTarget, target;
    Vector3 temp, cellSize , baseSIze;
    BlockDisplaySprite bdSprite;
    Tweener moveUp, scale;
    float scaleDuration, moveDuration, offsetY;
    bool init ;

    Vector2 scaleTarget, scaleStart;
    Camera cam ;
    void Start()
    {
        cam = UIManager.instance.mainCamera ;
    }
    public void  OnRespawn()
    {
        moveDuration = GameSetting.instance.moveDuration;
        scaleDuration = GameSetting.instance.scaleDuration;
        offsetY = GameSetting.instance.offset;
        //------------------------------------------------------
        bdSprite = GetComponent<BlockDisplaySprite>();
        cellSize = MyGrid2D.instance.Grid.cellSize;
        baseSIze = bdSprite.usingGrid.cellSize;
        scaleTarget = new Vector3(cellSize.x / baseSIze.x * 0.75f, cellSize.y / baseSIze.y * 0.75f, 1);

        isMouseUp = false;
        isDrag = false;
        isMouseDown = false ;
        
        scaleStart = Vector3.one ;
        moveStart = transform.position;
        moveTarget = new Vector3(transform.position.x, transform.position.y + offsetY, 0);
        transform.localScale = Vector3.one ;
        beginPos = transform.position ;
    }
    //public void OnRespawn()
    //{
        // cellSize = MyGrid2D.instance.Grid.cellSize;
        // baseSIze = bdSprite.usingGrid.cellSize;
        // scaleStart = Vector3.one ;
        // moveStart = transform.position;
        // moveTarget = new Vector3(transform.position.x, transform.position.y + offsetY, 0);
        
        // transform.localScale = Vector3.one ;
    //}
    void FixedUpdate()
    {
        if (!init)
        {
            temp = transform.position;
            return;
        }



        if ((temp - transform.position).magnitude < 0.01f)
        {
            isOnDrag = false;
            return;
        }
        else
        {
            isOnDrag = true;
            temp = transform.position;
        }
    }
    public void OnMouseDown()
    {
        if(!GameManager.instance.isGamePlay) return ;
        if (!bdSprite.isCanSnap) return;
        if(isMouseDown) return ;
        if(isDrag) return ;
        if(Input.touchCount > 1) return ;
        isOnDrag = false;
        init = true;
        isMouseDown = true ;
        isMouseUp = false ;

        #if PLATFORM_ANDROID && !UNITY_EDITOR
        begin_mPos = cam.ScreenToWorldPoint(Input.GetTouch(0).position);
        #endif
        #if UNITY_EDITOR
        begin_mPos = cam.ScreenToWorldPoint(Input.mousePosition);
        #endif
        delta = beginPos - m_Pos;
        target = moveTarget;
        lastPosTarget = transform.position;

        cellSize = bdSprite.blockSize / bdSprite.size;
        bdSprite.ChangeBlockSize(cellSize, cellSize * 0.15f);

        moveUp = transform.DOMove(moveTarget, moveDuration);
        scale = transform.DOScale(scaleTarget, scaleDuration);
    }
    
    public void OnMouseUp()
    {
        if(isMouseUp) return ;
        isMouseUp = true ;
        init = false;
        bdSprite.grid.cellGap = Vector3.zero;
        
        if ( moveUp != null && moveUp.IsActive()) moveUp.Kill();
                
        cellSize = bdSprite.blockSize / bdSprite.size;
        bdSprite.ChangeBlockSize(cellSize, Vector3.zero);
               // moveUp.ChangeStartValue(transform.position);
        moveUp = transform.DOMove(moveStart, moveDuration) ;
        scale = transform.DOScale(scaleStart , scaleDuration);
                
                // -----------------------------------
        isDrag = false;
        PopUpManager.instance.isDragBlock = false ;
        isMouseDown = false ;
    }
    void OnMouseDrag()
    {
        if(!GameManager.instance.isGamePlay) return ;
        if(!bdSprite.isCanSnap) return ;
        if(!isMouseDown) return ;
        if(Input.touchCount > 1) return ;
        #if PLATFORM_ANDROID && !UNITY_EDITOR
        m_Pos = cam.ScreenToWorldPoint(Input.GetTouch(0).position);
        #endif
        #if UNITY_EDITOR
         m_Pos = cam.ScreenToWorldPoint(Input.mousePosition);
        #endif
        if((m_Pos - begin_mPos).magnitude < 0.03f) return ;
        if(!isDrag)
        {
            lastPosTarget = transform.position;
            transform.position = lastPosTarget + m_Pos - begin_mPos ;
            last_mPos = m_Pos ;
            delta = lastPosTarget - begin_mPos  ;
            isDrag = true ;
            PopUpManager.instance.isDragBlock = true ;
            moveUp.Kill();
        }
        else
        {
            transform.position   = m_Pos +  delta ;
        }
    }

    public void OnDeSpawn()
    {
         moveUp.Kill() ;
         scale.Kill();
         gameObject.SetActive(false);
    }

    void CheckBlockSnap()
    {
        if(bdSprite.isSnap == true)
        {
            if(moveUp.IsActive()) moveUp.Kill(true);
            if(scale.IsActive()) scale.Kill(true);
            gameObject.SetActive(false);
        }
    }
}
