﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System.Linq;
public class BlockDrag : MonoBehaviour
{
    BlockClick blockClick;
    BlockDisplaySprite bdSprite;
    MyGrid2D myGrid2D;
    List<Vector2Int> lastSnapPosList, currentSnapPosList, SnapClearObjectsPos, cellSnapList;
    List<int> lastSnapRows, lastSnapCols, currentSnapRows, currentSnapCols;
    Sprite snapBlockSprite;
    bool isSnapChange, isSnap, isDrag, isClearChange;
    List<List<Vector2Int>> posIntList;
    int blockCount;

    void Start()
    {
        myGrid2D = MyGrid2D.instance;
       
        SnapClearObjectsPos = new List<Vector2Int>(100);
        cellSnapList = new List<Vector2Int>(25);
        lastSnapRows = new List<int>(10);
        lastSnapCols = new List<int>(10);
        currentSnapRows = new List<int>(10);
        currentSnapCols = new List<int>(10);
        blockClick = GetComponent<BlockClick>();
    }

    public void OnRespawn()
    {
        snapBlockSprite = transform.GetChild(0).GetComponent<SpriteRenderer>().sprite;
        lastSnapPosList = null;
        currentSnapPosList = null;
        bdSprite = GetComponent<BlockDisplaySprite>();
        posIntList = GameManager.instance.game.FindSnapPos(bdSprite.info);
        isSnap = bdSprite.isSnap;
    }
    void FixedUpdate()
    {
        if (!blockClick.isOnDrag) return;
        if(!GameManager.instance.isGamePlay) return ;
        currentSnapPosList = myGrid2D.getSnapPosList(bdSprite);

        if (currentSnapPosList == null)
        {
            if (lastSnapPosList != null)
            {
                ClearSnapBlockOnDrag(lastSnapPosList);
                lastSnapPosList = null ;
            } 
            SnapClearEnd();
            return;
        }
        else if (currentSnapPosList != null && currentSnapPosList.Count == blockCount)
        {
            if (lastSnapPosList == null)
            {
                isSnapChange = true;
            }
            else
            {
                isSnapChange = false;
                for (int i = 0; i < blockCount; i++)
                {
                    if (lastSnapPosList[i] - currentSnapPosList[i] != Vector2Int.zero)
                    {
                        isSnapChange = true;
                        break;
                    }
                }
            }
        }
        //SnapClearEnd();
        if (isSnapChange)
        {
            if (currentSnapPosList != null)
            {
                tryData();
                currentSnapRows = GameManager.instance.game.rowsToClear.ToList();
                currentSnapCols = GameManager.instance.game.colsToClear.ToList();
                if (lastSnapPosList != null) ClearSnapBlockOnDrag(lastSnapPosList);
                lastSnapPosList = currentSnapPosList.ToList();
            }
            isClearChange = isSnapClearChange();
            
        }
        else
        {
            if (currentSnapPosList != null) DisplaySnapBlockOnDrag(currentSnapPosList);
            isClearChange = false;
        }
        if (isClearChange)
        {
            SnapClearEnd();
            lastSnapRows = currentSnapRows.ToList();
            lastSnapCols = currentSnapCols.ToList();
        }
        else
        {
            SnapClear();
        }
        
    }
    public void OnMouseDown()
    {
        if(!GameManager.instance.isGamePlay) return ;
        if (!bdSprite.isCanSnap) return;
        posIntList = GameManager.instance.game.FindSnapPos(bdSprite.info);
        if (posIntList != null && posIntList.Count == 1) MyGrid2D.OnShowHintCell(posIntList[0]);
        lastSnapPosList = myGrid2D.getFirstSnapPos(posIntList);
        blockCount = bdSprite.blocksCellPos.Count;
        GameManager.instance.game.rowsToScore.Clear();
        GameManager.instance.game.colsToScore.Clear();
        isSnapChange = true;
    }

    public void OnMouseUp()

    {
        if(!GameManager.instance.isGamePlay) return ;
        if (currentSnapPosList != null && currentSnapPosList.Count == blockCount)
        {
            bdSprite.isSnap = true;
            for (int i = 0; i < currentSnapPosList.Count; i++)
            {
                GridCellUI cUI = MyGrid2D.cellUIs[currentSnapPosList[i].x, currentSnapPosList[i].y];
                cellSnapList.Add(new Vector2Int(cUI.i, cUI.j));
            }
            //Vector2Int v = cellSnapList[0];
            //Vector3 d = MyGrid2D.cellUIs[v.x, v.y].transform.position;
            //Vector3 D = bdSprite.GetBlockCell()[0].transform.position;
            //transform.position = d + transform.position - D;


            SnapClearEnd();
            ClearSnapBlockOnDrag(lastSnapPosList);
            setData();
            bdSprite.GetComponent<BlockClick>().OnDeSpawn();
        }
        else
        {
           // Vector2 cellSize = UIManager.instance.myGrid2D.Grid.cellSize;
           // Vector3 s = Vector3.one;
           // Transform bdT = bdSprite.usingGrid.transform;
           // transform.DOMove(bdT.position, 0.25f);
           // transform.DOScale(s, 0.25f);
        }
        if (posIntList != null && posIntList.Count == 1) MyGrid2D.OnClearHintCell(posIntList[0]);
    }
    void SnapBlockOnDrag()
    {

    }
    void DisplaySnapBlockOnDrag(List<Vector2Int> posList)
    {
        foreach (Vector2Int v in posList)
        {
            GridCellUI cellUI = MyGrid2D.cellUIs[v.x, v.y];
            Image img = cellUI.snapImg;
            img.color = new Color(1f, 1f, 1f, 0.5f);
            img.sprite = snapBlockSprite;
        }
    }
    void ClearSnapBlockOnDrag(List<Vector2Int> posList)
    {
        foreach (Vector2Int v in posList)
        {
            GridCellUI cellUI = MyGrid2D.cellUIs[v.x, v.y];
            Image img = cellUI.snapImg;
            img.color = new Color(0f, 0f, 0f, 0f);
        }
    }
    private void tryData()
    {
        List<Vector2Int> list = new List<Vector2Int>();
        for (int i = 0; i < blockCount; i++)
        {
            GridCellUI c = MyGrid2D.cellUIs[currentSnapPosList[i].x, currentSnapPosList[i].y];
            // c.blockObject = bdUI.blockObjs[i];

            list.Add(new Vector2Int(c.i, c.j));
        }
        GameManager.instance.game.tryData(list, 1);
        GameManager.instance.game.backUpData();
        if (Equipment.OnPlayerCheckClearEvent != null)
            Equipment.OnPlayerCheckClearEvent.
            Invoke(GameManager.instance.game.rowsToScore, GameManager.instance.game.colsToScore);
    }
    void setData()
    {
        GameManager.instance.game.scorePos = Vector3.zero;
        GridCellUI.moveID++;
        List<Vector2Int> list = new List<Vector2Int>();
        for (int i = 0; i < blockCount; i++)
        {
            GridCellUI c = MyGrid2D.cellUIs[currentSnapPosList[i].x, currentSnapPosList[i].y];
            // c.blockObject = bdUI.blockObjs[i];
            //Debug.Log(c.cellInfo == null);
            c.cellInfo.color = bdSprite.info.color;
            c.cellInfo.type = EquipmentType.Normal;
            c.blockImg.sprite = UIManager.instance.blockSprites[(int)c.cellInfo.color];
            c.blockImg.color = Color.white;
            list.Add(new Vector2Int(c.i, c.j));
            c.value = 1;
            c.cellID = GridCellUI.moveID;

            GameManager.instance.currentInfo.gameData[c.i * GameManager.instance.currentInfo.size.y + c.j].type = EquipmentType.Normal ;
        }
        PuzzleGame.scoreColor = bdSprite.info.color;
        GameManager.instance.game.setData(list, 1);
        
        Clear();
        if (BlockDisplaySprite.GameDataChangeEvent != null)
        {
            BlockDisplaySprite.GameDataChangeEvent.Invoke();
        }
        
        
        GameManager.instance.game.SpawnEquipmentCounter();
       
        UIManager.instance.UpdateScoreEvent.Invoke(GameManager.instance.game.GetScore());
        GameManager.instance.setBlockEvent.Invoke(bdSprite.slotIndex);
        
        GameManager.instance.UpdateCurrentGamePlayInfo();
        if (Equipment.OnPlayerClearEvent != null) Equipment.OnPlayerClearEvent.Invoke(GameManager.instance.game.rowsToClear, GameManager.instance.game.colsToClear);
    }

    #region  clear 

    void ClearRow(int row)
    {
        int min = MyGrid2D.gridSize.y, max = 0;
        foreach (Vector2Int v in cellSnapList)
        {
            if (v.y == row)
            {
                if (v.x < min) min = v.x;
                if (v.x > max) max = v.x;
            }
        }
        Vector2Int pos = new Vector2Int((min + max) / 2, row);
        GameObject ob = ObjectPooler.instance.SpawnFromPool("ClearHandler",transform.position);
        //ob.transform.SetParent(UIManager.instance.equipmentT);
        ClearHandler ch = ob.GetComponent<ClearHandler>();
        ch.pos = pos;
        ch.OnSpawn();
        ch.OnClearRow();
    }
    void ClearCol(int col)
    {
        int min = MyGrid2D.gridSize.x, max = 0;
        foreach (Vector2Int v in cellSnapList)
        {
            if (v.x == col)
            {
                if (v.y < min) min = v.y;
                if (v.y > max) max = v.y;
            }
        }
        Vector2Int pos = new Vector2Int(col, (min + max) / 2);
        GameObject ob = ObjectPooler.instance.SpawnFromPool("ClearHandler",transform.position);
        //ob.transform.SetParent(UIManager.instance.equipmentT);
        ClearHandler ch = ob.GetComponent<ClearHandler>();
        ch.pos = pos;
        ch.OnSpawn();
        ch.OnClearCol();
    }
    void Clear()
    {

        if (GameManager.instance.game.rowsToScore.Count == 0 && GameManager.instance.game.colsToScore.Count == 0)
            return;
        foreach (int row in GameManager.instance.game.rowsToScore)
        {
            ClearRow(row);
        }
        foreach (int col in GameManager.instance.game.colsToScore)
        {
            ClearCol(col);
        }
    }
    #endregion
    #region  SnapClear
    void SnapClearRow(int row)
    {
        int size = GameManager.instance.size.y;

        BlockColor color = bdSprite.info.color;
        string tag = color.ToString() + "Snap";
        for (int iCol = 0; iCol < size; iCol++)
        {
            GridCellUI cUI = MyGrid2D.cellUIs[iCol, row];
            if (cUI.value == 0) continue;

            cUI.blockImg.color = Color.clear;

            if (cUI.snapClearObject == null)
            {
                cUI.snapClearObject = ObjectPooler.instance.SpawnFromPool(tag, cUI.transform.position);
                cUI.snapClearObject.SetActive(true);
                cUI.snapClearColor = color;
            }
            else if (cUI.snapClearColor != color && !(cUI.snapClearObject == null))
            {
                cUI.snapClearObject.SetActive(false);
                cUI.snapClearObject = ObjectPooler.instance.SpawnFromPool(tag, cUI.transform.position);
                cUI.snapClearObject.SetActive(true);
                cUI.snapClearColor = color;
            }
            else if (cUI.snapClearColor == color && !cUI.snapClearObject.activeSelf)
            {
                cUI.snapClearObject.SetActive(true);
            }
            RectTransform rect1 = cUI.GetComponent<RectTransform>(),
                          rect2 = cUI.snapClearObject.GetComponent<RectTransform>();
            rect2.localScale = rect1.sizeDelta / rect2.sizeDelta;
            SnapClearObjectsPos.Add(new Vector2Int(iCol, row));
        }
    }
    void SnapClearCol(int col)
    {
        int size = GameManager.instance.size.x;
        for (int iRow = 0; iRow < size; iRow++)
        {
            BlockColor color = bdSprite.info.color;
            string tag = color.ToString() + "Snap";
            GridCellUI cUI = MyGrid2D.cellUIs[col, iRow];


            if (cUI.value == 0) continue;
            cUI.blockImg.color = Color.clear;

            if (cUI.snapClearObject == null)
            {
                cUI.snapClearObject = ObjectPooler.instance.SpawnFromPool(tag, cUI.transform.position);
                cUI.snapClearObject.SetActive(true);
                cUI.snapClearColor = color;

            }
            else if (cUI.snapClearColor != color && !(cUI.snapClearObject == null))
            {
                cUI.snapClearObject.SetActive(false);
                cUI.snapClearObject = ObjectPooler.instance.SpawnFromPool(tag, cUI.transform.position);
                cUI.snapClearObject.SetActive(true);
                cUI.snapClearColor = color;
            }
            else if (cUI.snapClearColor == color && !cUI.snapClearObject.activeSelf)
            {
                cUI.snapClearObject.SetActive(true);
            }
            SnapClearObjectsPos.Add(new Vector2Int(col, iRow));
            RectTransform rect1 = cUI.GetComponent<RectTransform>(),
                          rect2 = cUI.snapClearObject.GetComponent<RectTransform>();
            rect2.localScale = rect1.sizeDelta / rect2.sizeDelta;
        }
    }
    void SnapClear()
    {

        if (GameManager.instance.game.rowsToScore.Count == 0 && GameManager.instance.game.colsToScore.Count == 0)
            return;
        List<int> rows = GameManager.instance.game.rowsToClear.ToList(),
                  cols = GameManager.instance.game.colsToClear.ToList();

        foreach (int row in rows)
        {
            SnapClearRow(row);
        }
        foreach (int col in cols)
        {
            SnapClearCol(col);
        }

        //Debug.Break();
    }
    void SnapClearEnd()
    {
        //if (GameManager.instance.game.rowsToClear.Count != 0 || GameManager.instance.game.colsToClear.Count != 0)
        {
            for (int i = 0; i < SnapClearObjectsPos.Count; i++)
            {
                Vector2Int v = SnapClearObjectsPos[i];
                GridCellUI c = MyGrid2D.cellUIs[v.x, v.y];
                GameObject snapObject = c.snapClearObject;
                c.blockImg.color = Color.white;
                if ((!(snapObject == null)) && snapObject.activeSelf)
                {
                    snapObject.SetActive(false);
                    snapObject = null;
                }
            }
        }
        SnapClearObjectsPos.Clear();
    }

    bool isSnapClearChange()
    {
        if (currentSnapRows.Count != lastSnapRows.Count)
            return true;
        if (currentSnapCols.Count != lastSnapCols.Count)
            return true;
        for (int i = 0; i < lastSnapRows.Count; i++)
        {
            if (!currentSnapRows.Contains(lastSnapRows[i]))
                return true;
        }
        for (int i = 0; i < lastSnapCols.Count; i++)
        {
            if (!currentSnapCols.Contains(lastSnapCols[i]))
                return true;
        }

        return false;
    }

    void DisableBlock()
    {
        gameObject.SetActive(false);
    }
    #endregion
}
