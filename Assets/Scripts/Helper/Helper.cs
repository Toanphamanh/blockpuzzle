﻿using System.Collections;
using DG.Tweening ;
using UnityEngine;

public static class Helper 
{
    public static Sequence FadeIn(this Transform tranform , float duration)
    {
        
        if(!tranform.gameObject.activeSelf) tranform.gameObject.SetActive(true);
        Sequence s = DOTween.Sequence().SetAutoKill(false).Pause() ;
        s.AppendCallback(() => tranform.localScale = Vector3.one * 0.2f )
            .Append(tranform.DOScale(Vector3.one * 1.2f , duration * 0.5f)) 
                .Append(tranform.DOScale(Vector3.one , duration * 0.5f)).Restart();
        return s ;
    }

    public static Sequence FadeOut(this Transform tranform , float duration)
    {

        tranform.localScale = Vector3.one;
        if(!tranform.gameObject.activeSelf) tranform.gameObject.SetActive(true);
        Sequence s = DOTween.Sequence().SetAutoKill(false).Pause() ;
        s.Append(tranform.DOScale(Vector3.one * 1.2f , duration * 0.5f)) 
            .Append(tranform.DOScale(Vector3.one  , duration * 0.5f))
                .AppendCallback(() => tranform.gameObject.SetActive(false));       
        return s;
    }

    public static Sequence FlyOut(this Transform tranform , float duration , Vector3 dir)
    {
        Vector3 target = tranform.position + dir.normalized * 2f ;

        Tweener t = tranform.DOMove(target ,duration * 0.8f) ;
        if(!tranform.gameObject.activeSelf) tranform.gameObject.SetActive(true);
         Sequence s = DOTween.Sequence().SetAutoKill(false).Pause() ;
         s.Append(tranform.DOMove(tranform.position - dir.normalized * 0.01f , duration * 0.2f))
            .Append(t);
        return s;
    }
}
