﻿using System.Collections.Generic;
using UnityEngine;

public class GameSetting : MonoBehaviour

{
    public static GameSetting instance ;
    [Header("Block Spawn")]
    public   List<BlockType> commonBlockList ;
    public float commonRate ;
    public   List<BlockType> rareBlockList ;
    public float rareRate ;
    public   List<BlockType> superRareBlockList ;
    public float superRareRate ;
    [Header("Animation")]
    public float duration ;
    [Header("Block Drag")]
    public float offset;
    public float moveDuration , scaleDuration ;

    public void OnStart()
    {
        if(instance == null ) instance = this ;

    }
}
