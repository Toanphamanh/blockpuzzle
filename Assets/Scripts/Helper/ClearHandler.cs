﻿using System.Collections;
using UnityEngine;

public class ClearHandler : MonoBehaviour , IPooledObject
{
    // Start is called before the first frame update
    public Vector2Int pos;
    [SerializeField]
    public int moveID ;
    float rocketclearSpeed = 0.03f;
    float time = 3f ;
    [SerializeField]
    float clearSpeed = 0.03f;

    public void OnRocketClear()
    {
        StartCoroutine(RocketClearRowLeft(0, pos.x, pos.y));
        StartCoroutine(RocketClearRowRight(pos.x, MyGrid2D.gridSize.x, pos.y));
        StartCoroutine(RocketClearColDown(0, pos.y, pos.x));
        StartCoroutine(RocketClearColUp(pos.y, MyGrid2D.gridSize.y, pos.x));
    }

    void FixedUpdate()
    {
        if(time > 0) time -= Time.deltaTime ;
        else OnDespawn();
    }
    public void OnClearRow()
    {
        StartCoroutine(ClearRowLeft(0, pos.x, pos.y));
        StartCoroutine(ClearRowRight(pos.x, MyGrid2D.gridSize.x, pos.y));
    }

    public void OnClearCol()
    {
        StartCoroutine(ClearColDown(0, pos.y, pos.x));
        StartCoroutine(ClearColUp(pos.y, MyGrid2D.gridSize.y, pos.x));
    }
    #region IEumerator
    IEnumerator RocketClearRowRight(int start, int end, int row)
    {
        for (int i = start; i < end; i++)
        {
            GridCellUI cUI = MyGrid2D.cellUIs[i, row];
            if(cUI.equipObject != null )
            {
                cUI.equipObject.GetComponent<Equipment>().doEffect();
                cUI.equipObject = null;
            } 
            if(cUI.cellInfo.type != EquipmentType.None)
            {
                cUI.cellInfo.type = EquipmentType.None;
                cUI.blockImg.color = new Color(0, 0, 0, 0);
            }

            cUI.value = 0;
            yield return new WaitForSeconds(rocketclearSpeed);
        }
    }
    IEnumerator RocketClearRowLeft(int start, int end, int row)
    {
        for (int i = end - 1; i >= start; i--)
        {
            GridCellUI cUI = MyGrid2D.cellUIs[i, row];
            if(cUI.equipObject != null )
            {
                cUI.equipObject.GetComponent<Equipment>().doEffect();
                cUI.equipObject = null;
            } 
            if(cUI.cellInfo.type != EquipmentType.None )
            {
                cUI.cellInfo.type = EquipmentType.None;
                cUI.blockImg.color = new Color(0, 0, 0, 0);
            }
            cUI.value = 0;
            yield return new WaitForSeconds(rocketclearSpeed);
        }
    }

    IEnumerator RocketClearColDown(int start, int end, int col)
    {
        for (int i = end - 1; i >= start; i--)
        {
            GridCellUI cUI = MyGrid2D.cellUIs[col, i];
            if(cUI.equipObject != null )
            {
                cUI.equipObject.GetComponent<Equipment>().doEffect();
                cUI.equipObject = null;
            } 
            if(cUI.cellInfo.type != EquipmentType.None )
            {
                cUI.cellInfo.type = EquipmentType.None;
                cUI.blockImg.color = new Color(0, 0, 0, 0);
            }
            cUI.value = 0;
            yield return new WaitForSeconds(rocketclearSpeed);
        }
    }

    IEnumerator RocketClearColUp(int start, int end, int col)
    {
        for (int i = start; i < end; i++)
        {
            GridCellUI cUI = MyGrid2D.cellUIs[col, i];
            if(cUI.equipObject != null)
            {
                cUI.equipObject.GetComponent<Equipment>().doEffect();
                cUI.equipObject = null;
            } 
            if(cUI.cellInfo.type != EquipmentType.None )
            {
                cUI.cellInfo.type = EquipmentType.None;
                cUI.blockImg.color = new Color(0, 0, 0, 0);
                
            }
            cUI.value = 0;
            yield return new WaitForSeconds(rocketclearSpeed);
        }
    }
    IEnumerator ClearRowRight(int start, int end, int row)
    {
        Debug.Log("clear row right");
        for (int i = start; i < end; i++)
        {
            GridCellUI cUI = MyGrid2D.cellUIs[i, row];
            if(cUI.equipObject != null)
            {
                
                cUI.equipObject.GetComponent<Equipment>().doEffect();
                
                cUI.equipObject = null;
            } 
            if(cUI.cellInfo.type != EquipmentType.None )
            {
                cUI.cellInfo.type = EquipmentType.None;
                cUI.blockImg.color = new Color(0, 0, 0, 0);
            }
            GameObject effect = ObjectPooler.instance.SpawnFromPool("blockClearEffect",cUI.transform.position);
            ParticleSystem[] ps ;
            ps = effect.GetComponentsInChildren<ParticleSystem>();
            if(ps!= null)
            {
                for(int j = 0 ; j < ps.Length ;j++)
                {
                    ParticleSystemRenderer psR = ps[j].GetComponent<ParticleSystemRenderer>();
                    psR.material = ObjectPooler.instance.additive[j][(int)PuzzleGame.scoreColor];
                }
                if(ps[0].isPlaying) ps[0].Stop();
                ps[0].Play();
            }
            cUI.value = 0;
            yield return new WaitForSeconds(clearSpeed);
            //effect.SetActive(false);
        }
    }

    IEnumerator ClearRowLeft(int start, int end, int row)
    {
        Debug.Log("clear row left");
        for (int i = end - 1; i >= start; i--)
        {
            GridCellUI cUI = MyGrid2D.cellUIs[i, row];
            if(cUI.equipObject != null )
            {
                cUI.equipObject.GetComponent<Equipment>().doEffect();
                
                cUI.equipObject = null;
            } 
            if(cUI.cellInfo.type != EquipmentType.None )
            {
                
                cUI.cellInfo.type = EquipmentType.None;
                cUI.blockImg.color = new Color(0, 0, 0, 0);
            }
            GameObject effect = ObjectPooler.instance.SpawnFromPool("blockClearEffect",cUI.transform.position);
            ParticleSystem[] ps ;
            ps = effect.GetComponentsInChildren<ParticleSystem>();
            if(ps!= null)
            {
                for(int j = 0 ; j < ps.Length ;j++)
                {
                    ParticleSystemRenderer psR = ps[j].GetComponent<ParticleSystemRenderer>();
                    psR.material = ObjectPooler.instance.additive[j][(int)PuzzleGame.scoreColor];
                }
                if(ps[0].isPlaying) ps[0].Stop();
                ps[0].Play();
            }
            cUI.value = 0;
            yield return new WaitForSeconds(clearSpeed);
            //effect.SetActive(false);
        }
    }

    IEnumerator ClearColUp(int start, int end, int col)
    {
        Debug.Log("clear col up");
        for (int i = start; i < end; i++)
        {
            GridCellUI cUI = MyGrid2D.cellUIs[col, i];
            if(cUI.equipObject != null )
            {
                cUI.equipObject.GetComponent<Equipment>().doEffect();
                
                cUI.equipObject = null;
            } 
            if(cUI.cellInfo.type != EquipmentType.None )
            {
                
                cUI.cellInfo.type = EquipmentType.None;
                cUI.blockImg.color = new Color(0, 0, 0, 0);
            }

           GameObject effect = ObjectPooler.instance.SpawnFromPool("blockClearEffect",cUI.transform.position);
            ParticleSystem[] ps ;
            ps = effect.GetComponentsInChildren<ParticleSystem>();
            if(ps!= null)
            {
                for(int j = 0 ; j < ps.Length ;j++)
                {
                    ParticleSystemRenderer psR = ps[j].GetComponent<ParticleSystemRenderer>();
                    psR.material = ObjectPooler.instance.additive[j][(int)PuzzleGame.scoreColor];
                }
                if(ps[0].isPlaying) ps[0].Stop();
                ps[0].Play();
            }
            cUI.value = 0;
            yield return new WaitForSeconds(clearSpeed);
            //effect.SetActive(false);
        }
    }

    IEnumerator ClearColDown(int start, int end, int col)
    {
        Debug.Log("clear col down");
        for (int i = end - 1; i >= start; i--)
        {
            GridCellUI cUI = MyGrid2D.cellUIs[col, i];
            if(cUI.equipObject != null )
            {
                cUI.equipObject.GetComponent<Equipment>().doEffect();
                cUI.value = 0;
                cUI.equipObject = null;
            } 
            if(cUI.cellInfo.type != EquipmentType.None )
            {
                
                cUI.cellInfo.type = EquipmentType.None;
                cUI.blockImg.color = new Color(0, 0, 0, 0);
            }
            GameObject effect = ObjectPooler.instance.SpawnFromPool("blockClearEffect",cUI.transform.position);
            ParticleSystem[] ps ;
            ps = effect.GetComponentsInChildren<ParticleSystem>();
            if(ps!= null)
            {
                for(int j = 0 ; j < ps.Length ;j++)
                {
                    ParticleSystemRenderer psR = ps[j].GetComponent<ParticleSystemRenderer>();
                    psR.material = ObjectPooler.instance.additive[j][(int)PuzzleGame.scoreColor];
                }
                if(ps[0].isPlaying) ps[0].Stop();
                ps[0].Play();
            }
            cUI.value = 0;
            yield return new WaitForSeconds(clearSpeed);
        }
    }
    public void OnSpawn()
    {
        time = 3f ;
    }

    public void OnDespawn()
    {
        StopAllCoroutines();
        gameObject.SetActive(false);
    }
    #endregion

}
