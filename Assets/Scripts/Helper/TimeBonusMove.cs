﻿using UnityEngine;

public class TimeBonusMove : MonoBehaviour , IPooledObject
{
    [SerializeField] float speed ;
    [SerializeField] float lifeTime ;
    float time ;
    TextNumberHelper tnh ;
    public int bonusTime ;
    public void OnSpawn()
    {
        time = lifeTime ;
        tnh = GetComponentInChildren<TextNumberHelper>();
        if(tnh != null) tnh.value = bonusTime ;
        tnh.Init();
        tnh.Display();
    }

    void FixedUpdate()
    {

        if(time <= 0)
        {
            gameObject.SetActive(false);
            return ;
        } 
        time -= Time.deltaTime ;
        transform.position = new Vector3(transform.position.x , transform.position.y + speed * Time.deltaTime , transform.position.z) ;
    }

    public void OnDespawn()
    {
        throw new System.NotImplementedException();
    }
}
