﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using DG.Tweening;

public class GridCellUI : MonoBehaviour
{
    public static int moveID ;
    public int cellID = 0 , dID , equimentCounter;
    public Vector3 worldPos;
    public Vector3 cellSize;
    public CellInfo cellInfo;

    public BlockColor snapClearColor ;
    public int i, j, value;
    // Use this for initialization
    public Image bgImg, snapImg, blockImg, blockEndImg , hintImg;
    public GameObject blockObject, snapObject, equipObject, blockEndObject , snapClearObject ,hintObject; 
    public bool isInit , isClear;
    public static UnityEvent ChangeCellSpriteEvent , MoveIdCountEvent;
    void Awake()
    {
        isInit = false;
        if (ChangeCellSpriteEvent == null)
        {
            ChangeCellSpriteEvent = new UnityEvent();
        }
        if(MoveIdCountEvent == null)
        {
            MoveIdCountEvent = new UnityEvent();
        }
        MoveIdCountEvent.AddListener(OnMoveIDCount);
        ChangeCellSpriteEvent.AddListener(OnChangeCellSprite);
    }

    private void OnMoveIDCount()
    {
        cellID = moveID ;
    }

    void Start()
    {
    }
    public void Init()
    {
        snapClearObject = null ;
        moveID = 0 ;
        if (isInit) return;
        isInit = true;
        bgImg = gameObject.GetComponent<Image>();
        bgImg.sprite = UIManager.instance.gridcellSprite;
        bgImg.rectTransform.localScale = Vector3.one;
        bgImg.rectTransform.sizeDelta = cellSize;
        bgImg.type = Image.Type.Filled ;
        bgImg.fillMethod = Image.FillMethod.Horizontal ;
        

        snapObject.transform.localPosition = Vector3.zero;
        snapObject.name = "Snap Object";
        snapImg = snapObject.GetComponent<Image>();
        snapImg.rectTransform.sizeDelta = cellSize * 0.75f ;
        snapImg.color = new Color(0f, 0f, 0f, 0f);

        hintObject.transform.localPosition = Vector3.zero ;
        hintObject.name = "hint" ;
        hintImg = hintObject.GetComponent<Image>() ;
        hintImg.rectTransform.sizeDelta = cellSize ;
        hintImg.sprite = UIManager.instance.hintCellSprite ;
        hintImg.color = Color.clear ;

        blockObject.transform.localPosition = Vector3.zero;
        blockImg = blockObject.GetComponent<Image>();
        blockImg.rectTransform.sizeDelta = cellSize;
        blockImg.color = new Color(0, 0, 0, 0);
        blockObject.name = "Block Object";
        // UIShiny uiShiny = blockObject.AddComponent<UIShiny>() ;
        //             uiShiny.width = 0.6f;
        //             uiShiny.rotation = 135f;
        //             uiShiny.softness = 0.6f;
        //             uiShiny.brightness = 0.2f;
        //             uiShiny.loop = true;
        //             uiShiny.gloss = 0.65f;
        //             //----------------
        //             uiShiny.duration = 1f;
        //             uiShiny.loopDelay = 20f;

        blockEndObject.transform.localPosition = Vector3.zero;
        blockEndImg = blockEndObject.GetComponent<Image>();
        blockEndImg.rectTransform.sizeDelta = cellSize;
        blockEndImg.color = new Color(0, 0, 0, 0);
    }

    public void changeCellSize()
    {
        bgImg.rectTransform.sizeDelta = cellSize;
        snapImg.rectTransform.sizeDelta = cellSize * 0.75f ;
        hintImg.rectTransform.sizeDelta = cellSize ;
        blockImg.rectTransform.sizeDelta = cellSize;
        blockEndImg.rectTransform.sizeDelta = cellSize;
    }
    private void OnChangeCellSprite()
    {
        if (this.cellInfo.type != 0)
        {
            blockEndImg.sprite = UIManager.instance.endCellSprite;
            blockEndImg.color = new Color(1, 1, 1, 0);
            blockEndImg.DOFade(0.75f, 1f);
        }
    }

    public void Display()
    {
        
        if (this.cellInfo == null) return;
        blockEndImg.color = Color.clear;

        if (cellInfo.type != EquipmentType.None)
        {
            
            blockImg.sprite = UIManager.instance.blockSprites[(int)cellInfo.color];
            blockImg.color = Color.white;
            if (cellInfo.type != EquipmentType.Normal)
            {
                if (cellInfo.type == EquipmentType.Timer && cellInfo.counter > 0)
                {

                    equipObject = ObjectPooler.instance.SpawnFromPool("Timer",transform.position);
                    StopWatch timer = equipObject.GetComponent<StopWatch>();
                    Debug.Log("Timer");
                    timer.setTime(cellInfo.counter);
                    timer.OnSpawn();
                }
                if (cellInfo.type == EquipmentType.Rocket)
                {
                    equipObject = ObjectPooler.instance.SpawnFromPool("RocketIdle" , transform.position);
                }
                if (cellInfo.type == EquipmentType.Bomb && cellInfo.counter >= 0)
                {
                    equipObject = ObjectPooler.instance.SpawnFromPool("Bomb" , transform.position);
                    Bomb bomb = equipObject.GetComponent<Bomb>();
                    bomb.setMoveCount(cellInfo.counter);
                }
               
                if(equipObject != null)
                {
                    Equipment eq = equipObject.GetComponent<Equipment>();
                
                    eq.pos = new Vector2Int(i, j);
                    IPooledObject pooledObj = eq.GetComponent<IPooledObject>() ;
                    if(pooledObj != null) pooledObj.OnSpawn();
                }
                
                //eq.transform.SetParent(transform);    
            }

        }
        else
        {
            blockImg.color = Color.clear;
            equipObject = null ;
        }
    }

    void OnDestroy()
    {

    }
}

