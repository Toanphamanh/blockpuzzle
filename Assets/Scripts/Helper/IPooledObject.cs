﻿
public interface IPooledObject 
{
    // Start is called before the first frame update
    void OnSpawn();
    void OnDespawn() ;
}
