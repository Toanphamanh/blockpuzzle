﻿using System;
using System.Collections;
using UnityEngine;
using DG.Tweening ;
using Random = UnityEngine.Random;

public class RollNumberHelper : MonoBehaviour
{
    GameObject[] rollers ;
    public int value ;
    int count ;
    [SerializeField] GameObject child ;
    RectTransform rect ;
    Char[] number ;
    float[] numberYPos ;
    void Start()
    {
        Init();
        StartCoroutine(DO());
    }
    
    void Init()
    {
        numberYPos = new float[11];
        int _value = value , d , _count = 0;
        number = new Char[10];
        while (_value > 0)
        {
            d = _value % 10 ;
            number[_count++] = (char)d ;
            _value = (int)(_value / 10) ;
        }
        count = _count ;
        Vector2 elementSize = child.GetComponent<RectTransform>().sizeDelta , 
                rollersSize = new Vector2 (elementSize.x * count , elementSize.y / 11 ) ;
        RectTransform childRect = child.GetComponent<RectTransform>();
        rect = GetComponent<RectTransform>();
        rect.sizeDelta = rollersSize ;
        Debug.Log(" size" +rect.sizeDelta.ToString());
        numberYPos[0] =  - elementSize.y / 2 + rollersSize.y / 2 ; 
        for(int i = 0 ; i < 11 ; i++)
        {
            numberYPos[i] = numberYPos[0] + i*rollersSize.y;
            Debug.Log(numberYPos[i].ToString());
        }
            
        childRect.anchoredPosition = new Vector2(rollersSize.x/2 - elementSize.x/2 , numberYPos[5]) ;
        for(int i = 0 ; i < count - 1 ; i++)
        {
            GameObject roller = Instantiate(child , transform) ;
            roller.name = (i+1).ToString();
            RectTransform rect = roller.GetComponent<RectTransform>() ;
            rect.anchoredPosition = childRect.anchoredPosition + (i+1) * elementSize.x * Vector2.left ;
        }
         
    }
    IEnumerator DO()
    {   
        float random ;
        for(int i = 0 ; i < count ; i++)
        {
            random = Random.Range(0.4f,0.8f);
           roller(i,random) ;
           yield return new WaitForSeconds(0.2f);
        }
    }

    void roller(int index , float duration)
    {
            float start = numberYPos[0]  , end = numberYPos[10] ;
            Transform roller = transform.GetChild(index) ;
            RectTransform rect = roller.GetComponent<RectTransform>() ;
            rect.DOAnchorPosY(start , duration).SetAutoKill(false).OnComplete(
                () => rect.DOAnchorPosY(end , duration).From() 
            ).SetLoops(-1) ;
    }
    IEnumerator RollAll()
    {
        
        for(int i = 0 ; i < count ; i++)
        {
            StartCoroutine(roll(true,i));
            yield return new WaitForSeconds(0.5f);
        }
    }

    IEnumerator roll(bool isUp , int index)
    {
        Transform roller = transform.GetChild(index) ;
        float start = numberYPos[0]  , end = numberYPos[10] ;
        RectTransform rect = roller.GetComponent<RectTransform>() ;
        float target = start;   
        float lastTarget = rect.anchoredPosition.y ;
        Tweener t = rect.DOAnchorPosY(0, 1f).SetAutoKill(false);
        while(Time.realtimeSinceStartup < 60)
        {
           
            if(lastTarget != target ) 
            {
                lastTarget = rect.anchoredPosition.y ;
            } 
            else
            {
                t.ChangeStartValue(end).Restart();
            }
            yield return new WaitForFixedUpdate();
        }   

        yield return null ;
    }
}
