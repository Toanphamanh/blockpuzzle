﻿using System.Collections.Generic;
using UnityEngine;

public class ObjectPooler : MonoBehaviour
{
    public static ObjectPooler instance ;
    [SerializeField] Transform particles ;
    void Awake()
    {
        if(instance == null) instance = this ;
    }
    [System.Serializable]
    public class Pool
    {
        public string tag ;
        public GameObject prefab ;
        public int size ;
        public int index ;
    }
    public List<Pool> pools ;
    public List<Material[]> additive  ;
    public Dictionary<string,Queue<GameObject>> poolDictionary;
    // Start is called before the first frame update

    void Start()
    {
        GameObject[] temp = new GameObject[pools.Count];
        poolDictionary = new Dictionary<string, Queue<GameObject>>();
        for(int i = 0 ; i < pools.Count ; i++)
        {
            GameObject pool = new GameObject(pools[i].tag) ;
            
            temp[pools[i].index] = pool ;
            Queue<GameObject> queuObj = new Queue<GameObject>(pools[i].size);
            for(int j = 0 ; j < pools[i].size ;j++)
            {
                GameObject obj = Instantiate(pools[i].prefab , pool.transform) ;
                obj.name = pools[i].tag + (j+1).ToString();
                obj.SetActive(false);
                queuObj.Enqueue(obj);
            }  
            poolDictionary.Add(pools[i].tag , queuObj) ;          
        }

        for(int i = 0 ; i < pools.Count ; i++)
        {
            temp[i].transform.SetParent(transform);
            temp[i].transform.localScale = Vector3.one;
        }
            


        additive = new List<Material[]>(5);
        additive.Add(Resources.LoadAll<Material>("Materials/Additive"));
        additive.Add(Resources.LoadAll<Material>("Materials/Additive1"));
        additive.Add(Resources.LoadAll<Material>("Materials/Additive2"));
        additive.Add(Resources.LoadAll<Material>("Materials/Additive3"));
        additive.Add(Resources.LoadAll<Material>("Materials/Additive4"));
    }
    public void deActiveAll()
    {
        foreach(string k in poolDictionary.Keys)
        {
            if(k!="GridCellPref")
            foreach(GameObject o in poolDictionary[k])
            {

                o.SetActive(false);
            }
        }

    }
    public GameObject SpawnFromPool(string tag , Vector3 pos) 
    {
        if(!poolDictionary.ContainsKey(tag))
        {
            Debug.LogWarning("Pool with tag " + tag + "doesn't exist") ;
            return null ;
        }
        else
        {
            GameObject obj = poolDictionary[tag].Dequeue();
            obj.SetActive(true);
            obj.transform.position = pos;
            poolDictionary[tag].Enqueue(obj);
            return obj;
        }     
    }

    public Transform getPool(int index)
    {
        return transform.GetChild(index) ;
    }
    // Update is called once per frame
}
