﻿using System.Collections;
using System.Collections.Generic;
using Coffee.UIExtensions;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Grid))]
public class MyGrid2D : MonoBehaviour
{
    public static MyGrid2D instance;
    //GameObject[,] cells ;
    public static GridCellUI[,] cellUIs;
    public bool isInit, isDoShiny , isResetGame;
    Grid grid;
    public static Vector2Int gridSize;
    List<Vector2Int>[] diagons;
    List<UIShiny>[] uiShinys;
    List<GridCellUI>[] cells ;
    List<Image>[] bgImgs , blockImgs ;
    public GameObject cellPref ;
    [SerializeField] Transform pool ;
    public Grid Grid
    {
        get
        {
            return grid;
        }
        set
        {
            grid = value;
        }
    }
    void Awake()
    {
        if (instance == null) instance = this;
        isInit = false;
    }

    public static void OnPlayerScore(ScoreType type, Vector3 pos, BlockColor color)
    {
        GameObject ob = ObjectPooler.instance.SpawnFromPool("scorePref", pos);
        ScoreUI sUI = ob.GetComponent<ScoreUI>();
        sUI.color = color;
        sUI.type = type;
        sUI.OnSpawn();
    }

    public static void ShowGridCell()
    {

    }


    // Use this for initialization
    public void InitGame()
    {
        InitGrid();
        InitCell();
        diagons = getDiagons();
        uiShinys = getDiagonsUIS();
        isResetGame = false ;
        getCellDiagons();
        StartRollOut();
        StartRollIn();
        StartShiny();
        
       
    }
    // Update is called once per frame

    void InitGrid()
    {
        float x, y;
        gridSize = GameManager.instance.currentInfo.size;
        grid = GetComponent<Grid>();
        RectTransform rt = (RectTransform)transform;
        x = rt.sizeDelta.x / gridSize.x;
        y = rt.sizeDelta.y / gridSize.y;
        grid.cellSize = new Vector3(x, y, 0);
        cellUIs = new GridCellUI[gridSize.x, gridSize.y];
        //gameData = new int[gridSize.x , gridSize.y] ;
    }

    void InitCell()
    {
        Vector3Int cellPos;

        int x, y;
        for (int i = 0; i < gridSize.x; i++)
            for (int j = 0; j < gridSize.y; j++)
            {
                x = -gridSize.x / 2 + i;
                y = -gridSize.y / 2 + j;
                cellPos = new Vector3Int(x, y, 0);
                GameObject cell = ObjectPooler.instance.SpawnFromPool("GridCellPref" , Vector3.zero ) ;
                cell.name = "";
                cellUIs[i, j] = cell.GetComponent<GridCellUI>();
                cellUIs[i, j].i = i;
                cellUIs[i, j].j = j;
                cellUIs[i, j].worldPos = grid.GetCellCenterWorld(cellPos);
                cell.transform.position = cellUIs[i, j].worldPos;
                cellUIs[i, j].cellSize = new Vector3(grid.cellSize.x, grid.cellSize.y, grid.cellSize.z);
                cell.GetComponent<RectTransform>().sizeDelta = grid.cellSize ;
                cellUIs[i, j].gameObject.name = string.Format("{0} - {1}", i, j);
                cellUIs[i, j].transform.SetParent(this.transform);
                cellUIs[i, j].Init();
                cellUIs[i,j].changeCellSize();
            }
    }

    public static void OnLoadCellInfo()
    {
        
        GamePlayInfo info = GameManager.instance.currentInfo;
        for (int i = 0; i < gridSize.x; i++)
            for (int j = 0; j < gridSize.y; j++)
            {
                
                GridCellUI c = cellUIs[i, j];
                c.cellInfo = info.gameData[i * gridSize.x + j];
                c.value = c.cellInfo.type == EquipmentType.None ? 0 : 1;
                c.Display();
            }
    }

    public void ResetCell()
    {
        for (int i = 0; i < gridSize.x; i++)
            for (int j = 0; j < gridSize.y; j++)
            {
                GridCellUI c = cellUIs[i, j];
                c.cellInfo.type = EquipmentType.None;
                c.value = 0;
                c.Display();
            }
    }
    public Vector3 getBlockSnapPos(Vector3 pos, Vector2Int blockSize)
    {
        Vector3Int cellPos = grid.WorldToCell(pos);
        Vector3 cellWorldPos = grid.CellToWorld(cellPos),
                cellCenterPos = grid.GetCellCenterWorld(cellPos);
        Vector3 rs;
        float x, y;
        x = (blockSize.x - gridSize.x) % 2 == 0 ? cellWorldPos.x : cellCenterPos.x;
        y = (blockSize.y - gridSize.y) % 2 == 0 ? cellWorldPos.y : cellCenterPos.y;
        rs = new Vector3(x, y, 0);
        return rs;
    }


    public Vector2 GetCellSize()
    {
        Vector2 rs = new Vector2(grid.cellSize.x, grid.cellSize.y);
        return rs;
    }

    public GridCellUI GetGridCellUI(Vector3 pos)
    {
        Vector3Int cellPos = grid.WorldToCell(pos);
        int x = cellPos.x + gridSize.x / 2, y = cellPos.y + gridSize.y / 2;
        if (x < 0 || y < 0 || x >= gridSize.x || y >= gridSize.y) return null;
        else return cellUIs[x, y];
    }

    public List<Vector2Int> getSnapPosList(BlockDisplaySprite bd)
    {
        int[,] data = GameManager.instance.game.gameData;

        List<Vector3> posList = bd.GetBlocksPos();
        int n = posList.Count, baseIndex = -1;
        List<Vector2Int> rs = new List<Vector2Int>(n);
        List<Vector3Int> block = bd.blocksCellPos;
        GridCellUI cellUI;
        Vector2Int basePoint = Vector2Int.zero;
        int dx, dy, x, y;
        for (int i = 0; i < n; i++)
        {
            cellUI = GetGridCellUI(posList[i]);
            if (cellUI == null) continue;
            x = cellUI.i; y = cellUI.j;
            if (data[x, y] != 0) continue;
            baseIndex = i;
        }
        if (baseIndex == -1) return null;
        cellUI = GetGridCellUI(posList[baseIndex]);
        x = cellUI.i; y = cellUI.j;
        basePoint = new Vector2Int(x, y);
        for (int i = 0; i < n; i++)
        {
            dx = block[i].x - block[baseIndex].x;
            dy = block[i].y - block[baseIndex].y;
            x = basePoint.x + dx;
            y = basePoint.y + dy;

            if (x < 0 || y < 0 || x >= gridSize.x || y >= gridSize.y) return null;
            if (data[x, y] != 0) return null;
            Vector2Int v = new Vector2Int(x, y);
            rs.Add(v);
        }
        if (rs.Count < n) return null;
        return rs;
    }

    public List<Vector2Int> getFirstSnapPos(List<List<Vector2Int>> list)
    {
        if (list == null) return null;
        List<Vector2Int> rs = new List<Vector2Int>(list[0].Count);
        foreach (Vector2Int v in list[0])
        {
            rs.Add(v);
        }
        return rs;
    }

    public static void OnShowHintCell(List<Vector2Int> list)
    {
        foreach (Vector2Int v in list)
        {
            cellUIs[v.x, v.y].hintImg.color = Color.white;
        }
    }
    public static void OnClearHintCell(List<Vector2Int> list)
    {
        foreach (Vector2Int v in list)
            cellUIs[v.x, v.y].hintImg.color = Color.clear;
    }

    public void StartShiny()
    {
        if(isDoShiny) return ; 
        isDoShiny = true;
        StartCoroutine(DoShiny());
    }

    public void StartRollOut()
    {
        StartCoroutine(DoRollOutBoard());
    }
    public void clearBoard()
    {
        for(int i = 0 ; i < gridSize.x ; i++)
            for(int j = 0 ; j < gridSize.y ; j++)
            {
                cellUIs[i,j].bgImg.fillAmount = 0f ;
                cellUIs[i,j].blockImg.color = Color.clear;
                if(cellUIs[i,j].equipObject != null)
                {
                    cellUIs[i,j].equipObject.SetActive(false);
                    cellUIs[i,j].equipObject = null ;
                }
                cellUIs[i,j].blockEndImg.color = Color.clear ;
            }
    }
    public void StartRollIn()
    {
        StartCoroutine(DoRollInBoard());
    }
    IEnumerator DoRollOutBoard()
    {
        while(true)
        {
            yield return new WaitUntil(() => GameManager.instance.isDisplay == true) ;
            int count = bgImgs.Length ;
            for (int i = 0; i < count; i++)
            {
                for (int j = 0; j < bgImgs[i].Count; j++)
                {
                    bgImgs[i][j].color = Color.white;
                    bgImgs[i][j].fillAmount = 0;
                }

            }
            for (int i = 0; i < count; i++)
            {
                yield return new WaitForSeconds(0.02f);
                StartCoroutine(fillImageOut(bgImgs[i],0.01f)) ;

            }
            GameManager.instance.isDisplay = false ;
        }
    }
    IEnumerator DoRollInBoard()
    {
        isResetGame = false ;
        while(true)
        {
            yield return new WaitUntil(() => isResetGame == true) ;
            int count = bgImgs.Length ;
            for (int i = count - 1; i >= 0; i--)
            {
                yield return new WaitForSeconds(0.02f);
                StartCoroutine(fillImageIn(bgImgs[i],0.01f)) ;
                StartCoroutine(clear(cells[i]));
            }
            isResetGame = false ;
            GameManager.instance.game.reset();
            
        }
    }
    IEnumerator clear(List<GridCellUI> cells)
    {
            for(int i = 0 ; i < cells.Count ; i++)
            {
                cells[i].blockImg.color = Color.clear;
                if(cells[i].equipObject != null)
                {
                    cells[i].equipObject.SetActive(false);
                    cells[i].equipObject = null ;
                } 
            }
        yield return null ;
       
    }
    IEnumerator fillImageOut(List<Image> list , float duration)
    {
        float time = 0f ;
        while(time < duration)
        {
            time += Time.deltaTime ;
            for (int j = 0; j < list.Count; j++)
                {
                    list[j].fillAmount = time / duration ;
                }
            yield return new WaitForEndOfFrame();
        }
        yield return null ;
    }
    IEnumerator fillImageIn(List<Image> list , float duration)
    {
        float time = duration ;
        while(time > 0)
        {
            time -= Time.deltaTime ;
            if(time < 0) time = 0;
            for(int i = 0 ; i < list.Count ; i++)
            {
                list[i].fillAmount = time / duration ;
            }
            yield return new WaitForEndOfFrame();
        }
        yield return null ;
    }
    IEnumerator DoShiny()
    {
        while(true)
        {
            yield return new WaitUntil(() => GameManager.instance.isGamePlay == true) ;
            yield return new WaitForSeconds(10f) ;
            int count = uiShinys.Length;
            for (int i = 0; i < count; i++)
            {
                yield return new WaitForSeconds(0.35f);
                for (int j = 0; j < uiShinys[i].Count; j++)
                {
                    int x = diagons[i][j].x, y = diagons[i][j].y;
                   
                    UIShiny uiS = uiShinys[i][j];
                    
                    if (!uiS.play) uiS.Play();
                }
            }
            yield return new WaitUntil(() => GameManager.instance.isGamePlay == false) ;
            for (int i = 0; i < count; i++)
            {
                for (int j = 0; j < uiShinys[i].Count; j++)
                {
                    int x = diagons[i][j].x, y = diagons[i][j].y;
                   
                    UIShiny uiS = uiShinys[i][j];
                    uiS.effectFactor = 0f ;
                    uiS.Stop();
                }
            }
            isDoShiny = false ;
        }
    }
    void getCellDiagons()
    {
        bgImgs = new List<Image>[diagons.Length];
        blockImgs = new List<Image>[diagons.Length] ;
        cells = new List<GridCellUI>[diagons.Length] ;
        for (int i = 0; i < diagons.Length; i++)
        {
            bgImgs[i] = new List<Image>(diagons[i].Count);
            blockImgs[i] = new List<Image>(diagons[i].Count);
            cells[i] = new List<GridCellUI>(diagons[i].Count) ;
            foreach (Vector2Int v in diagons[i])
            {
                GridCellUI c = cellUIs[v.x, v.y];
                c.dID = i;
                cells[i].Add(c);
                Image img = c.bgImg ;
                bgImgs[i].Add(img);
                img = c.blockImg ;
                blockImgs[i].Add(img);
            }
        }
    }
    List<UIShiny>[] getDiagonsUIS()
    {
        
        List<UIShiny>[] lists = new List<UIShiny>[diagons.Length];
        for (int i = 0; i < diagons.Length; i++)
        {
            lists[i] = new List<UIShiny>(diagons[i].Count);
            foreach (Vector2Int v in diagons[i])
            {
                GridCellUI c = cellUIs[v.x, v.y];
                c.dID = i;
                UIShiny uiS = c.blockObject.GetComponent<UIShiny>();
                lists[i].Add(uiS);
            }
        }
        return lists;
    }
    List<Vector2Int>[] getDiagons()
    {
        int index, n;
        n = gridSize.x * 2 - 1;
      
        List<Vector2Int>[] lists = new List<Vector2Int>[n];
        for (index = 0; index < gridSize.x; index++)
        {
            lists[index] = new List<Vector2Int>(index);
            for (int i = 0; i <= index; i++)
                lists[index].Add(new Vector2Int(i, i + gridSize.x - index - 1));
        }

        for (index = gridSize.x; index < n; index++)
        {
            lists[index] = new List<Vector2Int>(n - index);
            for (int i = index - gridSize.x + 1; i <= n - gridSize.x; i++)
                lists[index].Add(new Vector2Int(i, i - (index - gridSize.x + 1)));
        }

        return lists;
    }

    public void OnChangeCell()
    {
        for(int i = 0 ; i < gridSize.x; i ++)
            for(int j = 0 ; j < gridSize.y ; j++)
            {

                cellUIs[i,j].gameObject.SetActive(false);
                cellUIs[i,j].transform.SetParent(ObjectPooler.instance.getPool(16));
            }
    }

}


