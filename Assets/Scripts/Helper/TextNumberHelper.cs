﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class TextNumberHelper : MonoBehaviour
{
    int count ;
    public static List<Sprite[]> numberSpritesList;
    public Sprite[] numberSprites ;
    [SerializeField] float scale = 1f;
    Image[] images ;
    public int value ;
    Char[] number;
    float time , rollSpeed = 10f;
    public static UnityEventInt OnValueChangeEvent ;

    bool countdown , isCountStart;
    void Awake()
    {
        if(OnValueChangeEvent == null) OnValueChangeEvent = new UnityEventInt();
        OnValueChangeEvent.AddListener(this.OnValueChange);
        count = 0;
        
    }

    void OnEnable()
    {
        
    }
    void Start()
    {
       if(numberSpritesList == null || numberSpritesList.Count == 0) numberSpritesList = PopUpManager.instance.numberSpritesList ;
    }

    public void setUp()
    {
        isCountStart = false ;
        countdown = false ;
         
    }
    public void StartCountDown()
    {
        if(gameObject.activeInHierarchy && !countdown) 
        {
            StartCoroutine(Countdown()); 
            countdown = true;
            Debug.Log("Count");
        }
              
    }

    public void StopCountDown()
    {
       countdown = false;    
    }
    private void OnValueChange(int value)
    {
        this.value = value ;
        Init();
        Display();
    }

    IEnumerator Countdown ()
    {
        while(value > 0 && countdown)
        {
            time += Time.deltaTime;
            if(time > 1)
            {
                time = time - 1 ;
                value = value - 1 ;
                Init();
                Display() ;
            }
            yield return new WaitForFixedUpdate() ;
        }
        yield return null;
        countdown = false ;
    }
    public void Display()
    {
        int index ;
        if(numberSprites == null || numberSprites.Length == 0) numberSprites = numberSpritesList[5] ;
        if(number == null || number.Length == 0) return ;
        for(int i = 0 ; i < 10 ; i++)
        {
            if( i < count)
            {
                if(images[i] == null)
                {
                    GameObject Object = new GameObject() ;
                    Object.transform.SetParent(transform) ;
                    images[i] = Object.AddComponent<Image>() ;
                    Object.transform.localScale = Vector3.one * scale;
                }
           
                index = number[count - 1 - i];
                images[i].sprite = numberSprites[index];
                images[i].SetNativeSize();
                
            }
            
            else 
            {
                if(images[i] != null)
                Destroy(images[i].gameObject);
            }
             
        }
    }
    public void RollNumber(float endValue , float duration)
    {
        if(endValue - value > 0) StartCoroutine(RollUp(endValue , duration));
        else StartCoroutine(RollDown(endValue , duration)) ;
        Init() ;
        Display() ;
    }

    IEnumerator RollUp(float endValue , float time)
    {
        float delta = endValue - value , d , _value = (float)value;
        d = (delta * Time.deltaTime / time);
        
        while(_value < endValue)
        {
            _value = _value + d ;
            value = (int)_value;
            Init() ;
            Display() ;
            yield return new WaitForEndOfFrame();
        }
        value = (int)endValue ;
        Init();
        Display();
    }

    IEnumerator RollDown(float endValue , float time)
    {
        float delta = value - endValue , d , _value = (float)value;
        d = (delta * Time.deltaTime / time);
        while(value > endValue)
        {
            _value = (_value - d) ;
            value = (int)_value ;
            Init() ;
            Display() ;
            yield return new WaitForEndOfFrame();
        }
        value = (int)endValue ;
        Init();
        Display();
    }
    public void Init()
    {
        long _value = value , d ;
        number = new Char[10];
        if(images == null) images = new Image[10] ;
        count = 0 ;
        if(_value == 0)
        {
            number[count++] = (char)0;
            return;
        } 
        while (_value > 0)
        {
            d = _value % 10 ;
            number[count++] = (char)d ;
            _value = _value / 10 ;
        }
        
    }

    public void ChangeSprites(int index)
    {
        numberSprites = numberSpritesList[index];
    }
}
