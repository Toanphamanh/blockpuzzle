﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragMe : MonoBehaviour
{
    // Start is called before the first frame update
    Vector3 delta , m ;
    void OnMouseDown()
    {
        m = Camera.main.ScreenToWorldPoint(Input.mousePosition) ;
        delta = new Vector3(m.x - transform.position.x , m.y - transform.position .y , 0) ;
    }
    void OnMouseDrag()
    {
        m = Camera.main.ScreenToWorldPoint(Input.mousePosition) ;
        transform.position = new Vector3(m.x - delta.x , m.y - delta .y , 0) ;
    }
}
